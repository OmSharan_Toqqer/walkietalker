package com.toqqer.walkietalkie.activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.toqqer.walkietalkie.Constants.Constants;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.adapter.UserListAdapter;
import com.toqqer.walkietalkie.backgroundservice.FetchAllDeviceContacts;
import com.toqqer.walkietalkie.database.DatabaseHelper;
import com.toqqer.walkietalkie.group.CreateGroupActivity;
import com.toqqer.walkietalkie.model.User;

import java.util.ArrayList;

public class AddUserActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,View.OnClickListener {

    private ListView listView;
    private HorizontalScrollView hsv;
    private LinearLayout AddContactsLayout;
    private FloatingActionButton mFabnxt;
    private UserListAdapter mAdapter;
    private ArrayList<User> userContacts;
    private ArrayList<User> groupContacts;
    private int mGroupId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        InitilizeViews();

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            mGroupId = bundle.getInt("GROUP");
        }
        groupContacts = new ArrayList<>();
        userContacts = getAllUsersList();

        if (userContacts != null) {
            mAdapter = new UserListAdapter(userContacts, this);
            listView.setAdapter(mAdapter);
        }

        listView.setOnItemClickListener(this);
        mFabnxt.setOnClickListener(this);
    }
    private void InitilizeViews(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.chatListWindow_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Users");
        listView = (ListView) findViewById(R.id.all_users_chatList);
        hsv = (HorizontalScrollView)findViewById(R.id.hsv);
        AddContactsLayout = (LinearLayout)findViewById(R.id.add_createMeeting_LIN_selected_contacts);
        mFabnxt = (FloatingActionButton)findViewById(R.id.mFabNxt);

    }
    @Override
    public void onClick(View v) {
        if (v == mFabnxt){
            if (groupContacts.size()>=2){
                Intent intent = new Intent(AddUserActivity.this,CreateGroupActivity.class);
                intent.putExtra("GROUP_CONTACTS",groupContacts);
                startActivity(intent);
            }else {
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please select one contact", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mGroupId == 101) {
            if (userContacts.get(position).getIsGroupValue() == 0){
                userContacts.get(position).setIsGroupValue(1);
                addSelectedContact(userContacts.get(position), position);
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
            }else {
               int viewCounts = AddContactsLayout.getChildCount();
                for (int i=0;i<viewCounts;i++){
                    View vi = AddContactsLayout.getChildAt(i);
                    TextView text = (TextView)vi.findViewById(R.id.mAddUserName);
                    if (text.getText().toString().equalsIgnoreCase(userContacts.get(position).getUserName())){
                        userContacts.get(position).setIsGroupValue(0);
                        if (mAdapter != null) {
                            mAdapter.notifyDataSetChanged();
                        }
                        AddContactsLayout.removeView(vi);
                        groupContacts.remove(userContacts.get(position));

                        break;
                    }

                }
                if (viewCounts>=2)
                    mFabnxt.setVisibility(View.VISIBLE);
                else
                    mFabnxt.setVisibility(View.INVISIBLE);

            }
        } else {
            Log.e("UserIds",userContacts.get(position).getUserId());
            Intent chatRoom = new Intent(this, ConversationActivity.class);
            chatRoom.putExtra("status","last seen yesterday 08:45 PM");
            chatRoom.putExtra("user",userContacts.get(position));
            startActivity(chatRoom);
            finish();
        }
    }
    /*
  * When user select the Contacts it will be add views types of Contacts
  * */
    private void addSelectedContact(User user, final int position)
    {

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View child = inflater.inflate(R.layout.adapter_group_create, null, false);


        TextView conName=(TextView)child.findViewById(R.id.mAddUserName);
        ImageView mUserImg = (ImageView)child.findViewById(R.id.mAddUserImg);

        conName.setText(user.getUserName());
        child.setTag(user);
        groupContacts.add(user);

        child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View selectedChild) {
                User user = (User)selectedChild.getTag();
                userContacts.get(position).setIsGroupValue(0);
                if (mAdapter!=null){
                    mAdapter.notifyDataSetChanged();
                }
                AddContactsLayout.removeView(selectedChild);
                groupContacts.remove(user);

                int viewCount = AddContactsLayout.getChildCount();
                if (viewCount>=2)
                    mFabnxt.setVisibility(View.VISIBLE);
                else
                    mFabnxt.setVisibility(View.INVISIBLE);
            }
        });
        AddContactsLayout.addView(child);
        scroolright();
        int viewCount = AddContactsLayout.getChildCount();
        if (viewCount>=2)
            mFabnxt.setVisibility(View.VISIBLE);
        else
            mFabnxt.setVisibility(View.INVISIBLE);

    }
    private void scroolright(){
        hsv.postDelayed(new Runnable() {

            public void run() {
                hsv.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        }, 100L);
    }
    private ArrayList<User> getAllUsersList(){
        ArrayList<User> list = new ArrayList<>();
        Cursor mCur = DatabaseHelper.getInstance().getAllUsers();
        if (mCur!=null && mCur.moveToFirst()){
            do {

                User contacts = new User(mCur.getString(mCur.getColumnIndex(Constants.AU_USER_ID)),
                        mCur.getString(mCur.getColumnIndex(Constants.AU_USER_FIREBASE_TOKEN)),
                        mCur.getString(mCur.getColumnIndex(Constants.AU_USER_IMAGE)),
                        mCur.getString(mCur.getColumnIndex(Constants.AU_CONTACT)),
                        mCur.getString(mCur.getColumnIndex(Constants.AU_USER_NAME)));
                contacts.setIsGroupValue(0);
                list.add(contacts);

            }while (mCur.moveToNext());
        }
        return list;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_user_menu, menu);
        //Adding SearchView function
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setFocusable(true);
        search(searchView);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
             //Fetching all Contacts
            startService(new Intent(getBaseContext(), FetchAllDeviceContacts.class));


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

}
