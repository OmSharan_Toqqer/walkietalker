package com.toqqer.walkietalkie.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.toqqer.walkietalkie.Constants.Constants;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.adapter.ChatListAdapter;
import com.toqqer.walkietalkie.backgroundservice.FetchAllDeviceContacts;
import com.toqqer.walkietalkie.database.AppSharedPreferences;
import com.toqqer.walkietalkie.database.DatabaseHelper;
import com.toqqer.walkietalkie.group.Group;
import com.toqqer.walkietalkie.model.PeopleListData;
import com.toqqer.walkietalkie.model.User;
import com.toqqer.walkietalkie.model.UserChat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ChatListActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayout order_list_empty_view;
    private static String LOG_TAG = "CardViewActivity";
    private Button addUserBtn;
    Snackbar snackbar;
    private ArrayList<Group> listGroup;

    private ArrayList<UserChat> userChatsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.chatListWindow_toolbar);
        setSupportActionBar(toolbar);

        listGroup = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.chatList_recyclerView);
        order_list_empty_view = (LinearLayout) findViewById(R.id.order_list_empty_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);


        findViewById(R.id.add_user_fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addUser = new Intent(getApplicationContext(), AddUserActivity.class);
                startActivity(addUser);
            }
        });
    }
    private ArrayList<UserChat> getAllUsersChatList(){
        ArrayList<UserChat> list = new ArrayList<>();
        Cursor mCur = DatabaseHelper.getInstance().getAllChatUsers();
        if (mCur!=null && mCur.moveToFirst()){
            do {
                UserChat userChat = DatabaseHelper.getInstance().getUserChatMessages(mCur);
                list.add(userChat);
            }while (mCur.moveToNext());
            getListGroup();
        }
        return list;
    }
    private void getListGroup(){
        if (listGroup.size()!=0)
            listGroup.clear();
        FirebaseDatabase.getInstance().getReference().child("users/"+ AppSharedPreferences.getInstance().getUserMobileNumber()+"/group").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    HashMap mapListGroup = (HashMap) dataSnapshot.getValue();
                    Iterator iterator = mapListGroup.keySet().iterator();
                    while (iterator.hasNext()){
                        String idGroup = (String) mapListGroup.get(iterator.next().toString());
                        Group newGroup = new Group();
                        newGroup.id = idGroup;
                        listGroup.add(newGroup);
                    }
                    getGroupInfo(0);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void getGroupInfo(final int indexGroup){
        if(indexGroup == listGroup.size()){
            for (int i=0;i<listGroup.size();i++){
                UserChat chat= new UserChat();
                chat.setmGroupname(listGroup.get(i).groupInfo.get("name"));
                chat.setReceiverUId(listGroup.get(i).id);
                userChatsList.add(chat);
            }
            mAdapter.notifyDataSetChanged();
        }else {
            FirebaseDatabase.getInstance().getReference().child("group/"+listGroup.get(indexGroup).id).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getValue() != null){
                        HashMap mapGroup = (HashMap) dataSnapshot.getValue();
                        ArrayList<String> member = (ArrayList<String>) mapGroup.get("member");
                        HashMap mapGroupInfo = (HashMap) mapGroup.get("groupInfo");
                        for(String idMember: member){
                            listGroup.get(indexGroup).member.add(idMember);
                        }
                        listGroup.get(indexGroup).groupInfo.put("name", (String) mapGroupInfo.get("name"));
                        listGroup.get(indexGroup).groupInfo.put("admin", (String) mapGroupInfo.get("admin"));
                    }
                    Log.d("GroupFragment", listGroup.get(indexGroup).id +": " + dataSnapshot.toString());
                    getGroupInfo(indexGroup +1);


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        userChatsList = getAllUsersChatList();

        if (userChatsList.size()!=0 && userChatsList!=null){
            order_list_empty_view.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);

            mAdapter = new ChatListAdapter(userChatsList, getApplicationContext());
            mRecyclerView.setAdapter(mAdapter);

        }else {
            mRecyclerView.setVisibility(View.GONE);
            order_list_empty_view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_list_layout_menu, menu);
        //Adding SearchView function
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setFocusable(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(getApplicationContext(),ProfileActivity.class));

                return true;
            case R.id.action_invite:
                snackbar = Snackbar.make(findViewById(android.R.id.content),"Under Process",Snackbar.LENGTH_LONG);
                snackbar.show();
                return true;
            case R.id.action_group:
                Intent intent = new Intent(this,AddUserActivity.class);
                intent.putExtra("GROUP",101);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
