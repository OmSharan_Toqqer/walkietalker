package com.toqqer.walkietalkie.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.toqqer.walkietalkie.Constants.Constants;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.adapter.ChatMessagesRecyclerAdapter;
import com.toqqer.walkietalkie.adapter.OnlineUsersAdapter;
import com.toqqer.walkietalkie.animation.MicAnimation;
import com.toqqer.walkietalkie.chat.ChatPresenter;
import com.toqqer.walkietalkie.database.DatabaseHelper;
import com.toqqer.walkietalkie.model.Chat;
import com.toqqer.walkietalkie.model.OnlineUser;
import com.toqqer.walkietalkie.model.User;
import com.toqqer.walkietalkie.model.UserChat;
import com.toqqer.walkietalkie.ui.ViewProxy;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by lenovo on 10/30/2017.
 */

public class ConversationGroupActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemClickListener{
    private FeatureCoverFlow coverFlow;
    private ArrayList<OnlineUser> onlineUserList;
    private TextView timerTxt,mToobarUserName,mToolbarStatus;
    private ImageView mToolbarUserIcon;
    private View slideText;
    private View recordPanel;
    private RelativeLayout deletAudio, recAudio;
    private RecyclerView mRecyclerView;
    private FloatingActionButton mFab;
    private ExpandableRelativeLayout expandableRelativeLayout;
    private LinearLayoutManager mLayoutManager;

    private float startedDraggingY = -1;
    private float distCanMove = dp(80);

    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    private String AudioSavePathInDevice = null;
    private MediaRecorder mediaRecorder ;
    private Random random ;
    private String RandomAudioFileName = "walkietalkie";
    public static final int RequestPermissionCode = 1;
    private MediaPlayer mediaPlayer ;

    private ArrayList<UserChat> messagesArrayList = new ArrayList<>();
    private ChatMessagesRecyclerAdapter adapter;
    private Timer timer;
    private int mFabclick = -1;

    private Calendar calendar ;
    private SimpleDateFormat df;
    private ChatPresenter mChatPresenter;
    private User receiverUser;
    private File audioFilePath;

    //firebase objects
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        initilizeViews();
        storageReference = FirebaseStorage.getInstance().getReference();

        calendar = Calendar.getInstance();
        df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        random = new Random();

        if (onlineUserList!=null) {
            OnlineUsersAdapter OnlineuserAdapter = new OnlineUsersAdapter(this, onlineUserList);
            coverFlow.setAdapter(OnlineuserAdapter);
            coverFlow.setOnScrollPositionListener(onScrollListener());
        }

        final MicAnimation mic_animation = (MicAnimation) findViewById(R.id.mic_view);
        mic_animation.setPulseColor(Color.parseColor("#00aced"));
        mic_animation.setPulseCount(3);
        mic_animation.setPulseMeasure(MicAnimation.PulseMeasure.WIDTH);
        mic_animation.setIconHeight(120);
        mic_animation.setIconWidth(120);
        mic_animation.setAlpha(10);
        mic_animation.setIconRes(R.drawable.mic_rec_focus);
        mic_animation.setInterpolator(new LinearInterpolator());

        mic_animation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    if (checkPermission()) {
                        //visible layout
                        deletAudio.setVisibility(View.VISIBLE);
                        recAudio.setVisibility(View.VISIBLE);

                        mic_animation.startPulse();
                        // Slide to cancel option
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) slideText
                                .getLayoutParams();
                        params.leftMargin = dp(20);
                        slideText.setLayoutParams(params);
                        ViewProxy.setAlpha(slideText, 1);
                        startedDraggingY = -1;
                        // startRecording();
                        startrecord();
                        mic_animation.getParent()
                                .requestDisallowInterceptTouchEvent(true);
                        recordPanel.setVisibility(View.VISIBLE);
                    }else {
                        requestPermission();
                    }

                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {

                    deletAudio.setVisibility(View.GONE);
                    recAudio.setVisibility(View.GONE);

                    startedDraggingY = -1;
                    mic_animation.finishPulse();
                    stoprecord();
                    // stopRecording(true);
                } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    float x = event.getX();
                    if (x < -distCanMove) {
                        stoprecord();
                        // stopRecording(false);
                    }
                    x = x + ViewProxy.getX(mic_animation);
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) slideText
                            .getLayoutParams();
                    if (startedDraggingY != -1) {
                        float dist = (x - startedDraggingY);
                        params.leftMargin = dp(20) + (int) dist;
                        slideText.setLayoutParams(params);
                        float alpha = 1.0f + dist / distCanMove;
                        if (alpha > 1) {
                            alpha = 1;
                        } else if (alpha < 0) {
                            alpha = 0;
                        }
                        ViewProxy.setAlpha(slideText, alpha);
                    }
                    if (x <= ViewProxy.getX(slideText) + slideText.getWidth()
                            + dp(20)) {
                        if (startedDraggingY == -1) {
                            startedDraggingY = x;
                            distCanMove = (recordPanel.getMeasuredWidth()
                                    - slideText.getMeasuredWidth() - dp(48)) / 2.0f;
                            if (distCanMove <= 0) {
                                distCanMove = dp(80);
                            } else if (distCanMove > dp(80)) {
                                distCanMove = dp(80);
                            }
                        }
                    }
                    if (params.leftMargin > dp(30)) {
                        params.leftMargin = dp(30);
                        slideText.setLayoutParams(params);
                        ViewProxy.setAlpha(slideText, 1);
                        startedDraggingY = -1;
                    }
                }
                v.onTouchEvent(event);
                return true;
            }
        });
        getLocalDatabaseGroupmessages();
    }

    private void initilizeViews(){

        receiverUser = (User)getIntent().getExtras().getSerializable("user");

        Toolbar toolbar = (Toolbar) findViewById(R.id.chatWindow_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToobarUserName = (TextView)findViewById(R.id.mUserToolbarName);
        mToobarUserName.setText(receiverUser.getUserName());

        mToolbarUserIcon = (ImageView)findViewById(R.id.mUserToolbarIcon);
        mToolbarUserIcon.setImageResource(R.drawable.user1);

        mToolbarStatus = (TextView)findViewById(R.id.mToolbarstatus);
        mToolbarStatus.setText(getIntent().getStringExtra("status"));

        slideText = findViewById(R.id.slideText);

        timerTxt = (TextView) findViewById(R.id.timerTxt);
        recordPanel = findViewById(R.id.record_panel);
        TextView textView = (TextView) findViewById(R.id.slideToCancelTextView);
        textView.setText("SlideToCancel");

        deletAudio = (RelativeLayout) findViewById(R.id.slideText);
        recAudio = (RelativeLayout) findViewById(R.id.record_audio);
        mFab = (FloatingActionButton)findViewById(R.id.mFab);
        expandableRelativeLayout = (ExpandableRelativeLayout)findViewById(R.id.expandableLayout1);
        expandableRelativeLayout.expand();

        coverFlow = (FeatureCoverFlow) findViewById(R.id.cover_flow);

        settingDummyData();

        mRecyclerView = (RecyclerView)findViewById(R.id.chatWindowList);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mFab.setOnClickListener(this);
        coverFlow.setOnItemClickListener(this);


    }
    private void getLocalDatabaseGroupmessages(){

        //receiver user userid is group id
        ArrayList<UserChat> getGroupchat = DatabaseHelper.getInstance().getGroupAllMessages(receiverUser.getUserId());
        if (getGroupchat!=null && getGroupchat.size()!=0){
            adapter = new ChatMessagesRecyclerAdapter(getGroupchat,ConversationGroupActivity.this);
            mRecyclerView.setAdapter(adapter);
        }
        getGroupMessages();

    }
    private void getGroupMessages(){
        if (receiverUser.getUserId()!=null) {
            FirebaseDatabase.getInstance().getReference().child("Group_chat/" + receiverUser.getUserId()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (dataSnapshot.getValue() != null) {
                        Chat chat = dataSnapshot.getValue(Chat.class);
                        UserChat userChat = new UserChat(chat.senderUid,chat.receiverUid,chat.message,2,null,String.valueOf(chat.timestamp),2);

                        if (DatabaseHelper.getInstance().isValideGroupmessageTimestamp(String.valueOf(chat.timestamp)) == false) {
                            DatabaseHelper.getInstance().insert_group_messages(userChat);
                        }

                        if (adapter == null) {
                            adapter = new ChatMessagesRecyclerAdapter(new ArrayList<UserChat>(),ConversationGroupActivity.this);
                            mRecyclerView.setAdapter(adapter);
                        }
                        adapter.add(userChat);
                        adapter.notifyDataSetChanged();
                        mRecyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    public void MediaRecorderReady(){
        mediaRecorder=new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
        mediaRecorder.setAudioEncodingBitRate(16);
        mediaRecorder.setAudioSamplingRate(16000);
    }

    public String CreateRandomAudioFileName(int string){
        StringBuilder stringBuilder = new StringBuilder( string );
        int i = 0 ;
        while(i < string ) {
            stringBuilder.append(RandomAudioFileName.
                    charAt(random.nextInt(RandomAudioFileName.length())));

            i++ ;
        }
        return stringBuilder.toString();
    }

    private FeatureCoverFlow.OnScrollPositionListener onScrollListener() {
        return new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                Log.v("ConversationActivity", "position: " + position);
            }

            @Override
            public void onScrolling() {
                Log.i("ConversationActivity", "scrolling");
            }
        };
    }

    private void settingDummyData() {
        onlineUserList = new ArrayList<>();
        onlineUserList.add(new OnlineUser(R.drawable.user1, "Jacob"));
        onlineUserList.add(new OnlineUser(R.drawable.user2, "Liam"));
        onlineUserList.add(new OnlineUser(R.drawable.user3, "Adam"));
        onlineUserList.add(new OnlineUser(R.drawable.user4, "Leo"));
        onlineUserList.add(new OnlineUser(R.drawable.user5, "Felix"));
        onlineUserList.add(new OnlineUser(R.drawable.user6, "Carlo"));
        onlineUserList.add(new OnlineUser(R.drawable.user7, "Bob"));
        onlineUserList.add(new OnlineUser(R.drawable.user8, "Noha"));
        onlineUserList.add(new OnlineUser(R.drawable.user9, "Pj"));
        onlineUserList.add(new OnlineUser(R.drawable.user10, "Arjun"));
    }
    private void startrecord() {
        // TODO Auto-generated method stub
        startTime = SystemClock.uptimeMillis();
        timer = new Timer();
        MyTimerTask myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 1000, 1000);
        vibrate();

        AudioSavePathInDevice =
                Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                        "WT"+CreateRandomAudioFileName(5)+ "AudioRecording.3gp";

        MediaRecorderReady();

        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void stoprecord() {
        // TODO Auto-generated method stub
        if (timer != null) {
            timer.cancel();
        }
        if (timerTxt.getText().toString().equals("00:00")) {
            return;
        }
        timerTxt.setText("00:00");
        vibrate();
        Log.e("FilePath",AudioSavePathInDevice);

        if(mediaRecorder != null){
            mediaRecorder.stop();
            mediaRecorder.release();
            UploadFile();
        }
    }

    private void vibrate() {
        // TODO Auto-generated method stub
        try {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void UploadFile(){
        Uri uri = Uri.fromFile(new File(AudioSavePathInDevice));
        //getting the storage reference
        StorageReference sRef = storageReference.child(Constants.STORAGE_AUDIO_FILES + System.currentTimeMillis() + "." + "mp3");
        sRef.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.e("Savefile",taskSnapshot.getDownloadUrl().toString());
                SendToServerMessages(taskSnapshot.getDownloadUrl().toString());
            }
        });
    }
    private void SendToServerMessages(String file_url){

        String audio_url = file_url;
        String receiver = receiverUser.getUserContact();
        String groupId = receiverUser.getUserId() ;
        String sender = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
        String senderUid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        Chat chat = new Chat(sender,
                receiver,
                senderUid,
                groupId,
                audio_url,
                System.currentTimeMillis());

        FirebaseDatabase.getInstance().getReference().child("Group_chat/" + groupId).push().setValue(chat);

    }
    public static int dp(float value) {
        return (int) Math.ceil(1 * value);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent chatRoom = getIntent();
        chatRoom.putExtra("text", onlineUserList.get(position).getName());
        chatRoom.putExtra("icon",onlineUserList.get(position).getImageSource());
        startActivity(chatRoom);
        finish();
    }
    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            final String hms = String.format(
                    "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(updatedTime)
                            - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
                            .toHours(updatedTime)),
                    TimeUnit.MILLISECONDS.toSeconds(updatedTime)
                            - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                            .toMinutes(updatedTime)));
            long lastsec = TimeUnit.MILLISECONDS.toSeconds(updatedTime)
                    - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
                    .toMinutes(updatedTime));
            System.out.println(lastsec + " hms " + hms);
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        if (timerTxt != null)
                            timerTxt.setText(hms);
                    } catch (Exception e) {
                        // TODO: handle exception
                    }

                }
            });
        }
    }
    private void requestPermission() {
        ActivityCompat.requestPermissions(ConversationGroupActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length> 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {

                    } else {
                        Toast.makeText(ConversationGroupActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.conversation_group_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
               Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Under Process", Snackbar.LENGTH_LONG);
                snackbar.show();
                return true;
            case R.id.action_invite:
               Snackbar snackbar1 = Snackbar.make(findViewById(android.R.id.content), "Under Process", Snackbar.LENGTH_LONG);
                snackbar1.show();
                return true;
            case R.id.action_groupinfo:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onClick(View v) {
        if (v == mFab){
            if (mFabclick == -1) {
                mFabclick = 1;
                mFab.setImageResource(R.drawable.ic_action_hide);
                expandableRelativeLayout.toggle();
            }else {
                mFabclick = -1;
                mFab.setImageResource(R.drawable.ic_action_show);
                expandableRelativeLayout.toggle();
            }
        }
    }

}
