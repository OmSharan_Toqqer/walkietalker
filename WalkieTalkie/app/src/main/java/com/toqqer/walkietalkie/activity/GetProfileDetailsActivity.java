package com.toqqer.walkietalkie.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.toqqer.walkietalkie.Constants.Constants;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.database.AppSharedPreferences;
import com.toqqer.walkietalkie.database.DatabaseHelper;
import com.toqqer.walkietalkie.model.Upload;
import com.toqqer.walkietalkie.model.User;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lenovo on 10/25/2017.
 */

public class GetProfileDetailsActivity extends AppCompatActivity {
    private ImageView mImage;
    private TextView mTxt,mMobileNum;
    //database reference
    private DatabaseReference mDatabase;

    private ArrayList<Upload> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.getimagelayout);

        mImage = (ImageView)findViewById(R.id.mImageView);
        mTxt = (TextView)findViewById(R.id.mTextview);
        mMobileNum = (TextView)findViewById(R.id.mMobileNumber);

        list = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        FirebaseDatabase.getInstance().getReference().child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterator<DataSnapshot> dataSnapshots = dataSnapshot.getChildren().iterator();
                while (dataSnapshots.hasNext()) {
                    DataSnapshot dataSnapshotChild = dataSnapshots.next();

                    Upload upload = dataSnapshotChild.getValue(Upload.class);

                    Log.e("sjancxjsnxjs",upload.getName());

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
