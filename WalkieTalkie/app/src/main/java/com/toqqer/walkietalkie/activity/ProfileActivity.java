package com.toqqer.walkietalkie.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.toqqer.walkietalkie.Constants.Constants;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.backgroundservice.FetchAllDeviceContacts;
import com.toqqer.walkietalkie.database.AppSharedPreferences;
import com.toqqer.walkietalkie.model.FilePath;
import com.toqqer.walkietalkie.model.Upload;
import com.toqqer.walkietalkie.ui.CircleTransform;
import com.toqqer.walkietalkie.ui.CropSquareTransformation;
import com.toqqer.walkietalkie.utils.UUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by lenovo on 9/25/2017.
 */

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{

    private int PROFILE_CAMERA = 1707;
    private int PROFILE_GALARY = 1708;

    private TextView mProfileHeadertxt,mProfileMessagetxt;
    private EditText mHintedittxt;
    private Button mNxtBtn;
    private ImageView profileBtn;

    private FloatingActionButton Btn_Gallery,Btn_Camera,Btn_Remove;
    private BottomSheetDialog mBottomsheetDilog;

    //Current local application:
    private Locale myLocale;

    private boolean result=false;
    private String userChoosenTask = null;
    private String selectedImagePath = null;
    private File mImageFile;

    //uri to store file
    private Uri filePath;

    //firebase objects
    private StorageReference storageReference;

    private FirebaseStorage mFirebaseStorage;
    private String previousImgUrl,Uid,MobileNumber ;
    private DatabaseReference mDatabaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initilizeViews();

        storageReference = FirebaseStorage.getInstance().getReference();
        mFirebaseStorage = FirebaseStorage.getInstance().getReference().getStorage();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        Uid = AppSharedPreferences.getInstance().getUserId();
        MobileNumber = AppSharedPreferences.getInstance().getUserMobileNumber();

        getPreviousProfileData();

        mNxtBtn.setOnClickListener(this);
        profileBtn.setOnClickListener(this);

    }
    private void initilizeViews(){
        mProfileHeadertxt = (TextView)findViewById(R.id.titleText);
        mProfileMessagetxt = (TextView)findViewById(R.id.inputFildDetail);
        mHintedittxt = (EditText)findViewById(R.id.profile_editText);
        profileBtn = (ImageView)findViewById(R.id.people_profile_image);
        mNxtBtn = (Button)findViewById(R.id.profile_nxtBtn);
        loadLocale();
        initilize_bottomsheet();
    }
    private void initilize_bottomsheet(){
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottomsheetlayout, null);
        Btn_Gallery = (FloatingActionButton)modalbottomsheet.findViewById(R.id.fab_gallary);
        Btn_Camera = (FloatingActionButton)modalbottomsheet.findViewById(R.id.fab_camera);
        Btn_Remove = (FloatingActionButton)modalbottomsheet.findViewById(R.id.fab_remove);
        mBottomsheetDilog = new BottomSheetDialog(ProfileActivity.this);
        mBottomsheetDilog.setContentView(modalbottomsheet);
        mBottomsheetDilog.setCanceledOnTouchOutside(true);
        mBottomsheetDilog.setCancelable(true);

        Btn_Remove.setOnClickListener(this);
        Btn_Camera.setOnClickListener(this);
        Btn_Gallery.setOnClickListener(this);

    }
    public void loadLocale()
    {
        String lang = AppSharedPreferences.getInstance().getlanguage();
        if (lang!=null){
            changeLang(lang);
        }
    }
    //change the layout laguage
    public void changeLang(String lang)
    {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        AppSharedPreferences.getInstance().saveLanguage(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }
    //add the language from perticlular fields
    private void updateTexts()
    {
        mProfileHeadertxt.setText(R.string.action_profile);
        mProfileMessagetxt.setText(R.string.aboutInputs);
        mHintedittxt.setHint(R.string.action_nick_name);
        mNxtBtn.setText(R.string.action_nxt);
    }

    //select Gallery
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),PROFILE_GALARY);
    }
    //select camera
    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, PROFILE_CAMERA);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case UUtils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Camera"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Gallery"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PROFILE_GALARY)
                onSelectFromGalleryResult(data);
            else if (requestCode == PROFILE_CAMERA)
                onCaptureImageResult(data);
        }
    }
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        filePath = data.getData();
        selectedImagePath = FilePath.getPath(this, data.getData());
        Picasso.with(getApplicationContext())
                .load(data.getData())
                .transform(new CropSquareTransformation())
                .placeholder(R.drawable.image_holder)
                .error(R.drawable.image_holder)
                .into(profileBtn);
        if (profileBtn.getVisibility() != View.VISIBLE) {
            profileBtn.setVisibility(View.VISIBLE);
        }
    }
    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Picasso.with(getApplicationContext())
                .load(destination.getAbsoluteFile())
                .transform(new CropSquareTransformation())
                .placeholder(R.drawable.image_holder)
                .error(R.drawable.image_holder)
                .into(profileBtn);
    }
    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }
    private void uploadFile() {


        //checking if file is available
        if (filePath != null) {
            //displaying progress dialog while image is uploading
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading");
            progressDialog.show();

            //getting the storage reference
            StorageReference sRef = storageReference.child(Constants.STORAGE_PATH_UPLOADS + System.currentTimeMillis() + "." + getFileExtension(filePath));

            //adding the file to reference
            sRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //dismissing the progress dialog
                            progressDialog.dismiss();
                            deletePreviousData();

                            //displaying success toast
                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();

                            String DeviceId = UUtils.getDeviceIMEI(ProfileActivity.this);
                            String FirebaseToken = AppSharedPreferences.getInstance().getFirebaseId();

                            //creating the upload object to store uploaded image details
                            Upload upload = new Upload(mHintedittxt.getText().toString().trim(),
                                    taskSnapshot.getDownloadUrl().toString(),
                                    DeviceId,MobileNumber,FirebaseToken,Uid);

                            //adding an upload to firebase database
                            mDatabaseRef.child(Constants.DATABASE_USERS)
                                    .child(MobileNumber+"/user")
                                    .setValue(upload)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.e("Message","success");

                                                Intent intent = new Intent(ProfileActivity.this, ChatListActivity.class);
                                                startActivity(intent);

                                            } else {
                                                Log.e("Message","Failure");
                                            }
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying the upload progress
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        } else {
            String DeviceId = UUtils.getDeviceIMEI(ProfileActivity.this);
            String MobileNumber = AppSharedPreferences.getInstance().getUserMobileNumber();
            String FirebaseToken = AppSharedPreferences.getInstance().getFirebaseId();

            //creating the upload object to store uploaded image details
            Upload upload = new Upload(mHintedittxt.getText().toString().trim(),
                    previousImgUrl,DeviceId,MobileNumber,FirebaseToken,Uid);

            //adding an upload to firebase database
            mDatabaseRef.child(Constants.DATABASE_USERS)
                    .child(MobileNumber+"/user")
                    .setValue(upload)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.e("Message","success");

                                Intent intent = new Intent(ProfileActivity.this, ChatListActivity.class);
                                startActivity(intent);

                            } else {
                                Log.e("Message","Failure");
                            }
                        }
                    });
        }
    }
    private void deletePreviousData(){
        mDatabaseRef.child("users").child(MobileNumber+"/user")
                .orderByChild(MobileNumber+"/user")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue()!= "" && dataSnapshot.getValue()!=null) {
                            Upload upload = dataSnapshot.getValue(Upload.class);


                            StorageReference photoRef = mFirebaseStorage.getReferenceFromUrl(upload.getUrl());
                            photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    // File deleted successfully
                                    Log.d("DeletePreviousImage", "onSuccess: deleted file");
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Uh-oh, an error occurred!
                                    Log.d("Fail", "onFailure: did not delete file");
                                }
                            });

                            DataSnapshot firstChild = dataSnapshot.getChildren().iterator().next();
                            firstChild.getRef().removeValue();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
    private void getPreviousProfileData(){
        //adding an event listener to fetch values
        mDatabaseRef.child("users").child(MobileNumber+"/user").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() != null && snapshot.getValue()!="123") {
                    Upload upload = snapshot.getValue(Upload.class);

                    mHintedittxt.setText(upload.getName());
                    previousImgUrl = upload.getUrl();

                    Glide.with(ProfileActivity.this).load(previousImgUrl)
                            .transform(new CircleTransform(ProfileActivity.this)).into(profileBtn);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == mNxtBtn) {
            uploadFile();
        }else if (v == profileBtn){
            mBottomsheetDilog.show();
        }else if (v == Btn_Gallery){
            userChoosenTask = "Gallery";
            result =  UUtils.checkPermission(ProfileActivity.this);
            if (result)
             galleryIntent();
            mBottomsheetDilog.dismiss();
        }else if (v == Btn_Camera){
            userChoosenTask = "Camera";
            result =  UUtils.checkCameraPermission(ProfileActivity.this);
            if (result)
             cameraIntent();
             mBottomsheetDilog.dismiss();
        }else if (v == Btn_Remove){
            Toast.makeText(this, "Under Process", Toast.LENGTH_SHORT).show();
            mBottomsheetDilog.dismiss();
        }
    }
}
