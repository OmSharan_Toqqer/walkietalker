package com.toqqer.walkietalkie.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.toqqer.walkietalkie.backgroundservice.FetchAllDeviceContacts;
import com.toqqer.walkietalkie.json.CountryLanguages;
import com.toqqer.walkietalkie.json.CountryNames;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.adapter.ChooseLangageAdapter;
import com.toqqer.walkietalkie.adapter.CountryCodeAdapter;
import com.toqqer.walkietalkie.database.AppSharedPreferences;
import com.toqqer.walkietalkie.json.JsonServer;
import com.toqqer.walkietalkie.model.Countries;
import com.toqqer.walkietalkie.model.Languages;
import com.toqqer.walkietalkie.model.Registration;
import com.toqqer.walkietalkie.network.RegisterService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemClickListener{
    View.OnClickListener mOnClickListener;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 200;

    private static final String TAG = "PhoneAuthActivity";

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    public static PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private EditText Ed_CountryCode, Ed_MobileNumber, Ed_Country;
    private TextView mWelcomeToqqer, mToqermessage, mChooseLang;

    private Button mRegisterBtn;
    private ImageView mChooseLanguageImgBtn;

    private ArrayList<Countries> countryList;
    private ArrayList<Languages> mAllLanguages;

    public ListView mListviewCountryCodes, mListviewLanguages;
    public Dialog mCountryCodesDilog, mCountryLanguagesDilog;
    public EditText searchCountry;

    public CountryCodeAdapter adapter;
    public ChooseLangageAdapter mLngAdapter;

    //Current local application:
    private Locale myLocale;
    private ProgressDialog mDilog;
    private String FireBaseToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //already registered this will be called
        if (AppSharedPreferences.getInstance().getIsLocked())
        {
            Intent intent=new Intent(RegisterActivity.this, ChatListActivity.class);
            startActivity(intent);
            finish();
        }

        setContentView(R.layout.activity_register);
        initilizeviews();
        permisions();

        mDilog = new ProgressDialog(RegisterActivity.this);
        mDilog.setCancelable(false);

        countryList = new ArrayList<>();
        mAllLanguages = new ArrayList<>();

        //country code create dilog method
        AppcountryCode();

        //click listeners
        Ed_Country.setOnClickListener(this);
        mRegisterBtn.setOnClickListener(this);

        mChooseLanguageImgBtn.setOnClickListener(this);

        //country code search country textwatcher
        searchCountry.addTextChangedListener(new EditTextWatcher(searchCountry));

        //snackbar undo clicklistener
        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountryCodesDilog.show();
            }
        };

        loadLocale();
        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);

                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.e(TAG, "onCodeSent:" + verificationId);
                hideDialog();


                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                Intent intent = new Intent(RegisterActivity.this, VerifyOtpScreenActivity.class);
                intent.putExtra("MOBILENUMBER", Ed_MobileNumber.getText().toString());
                intent.putExtra("CODE", Ed_CountryCode.getText().toString());
                intent.putExtra("VERIFICATIONID",verificationId);
                startActivity(intent);
                finish();

            }
        };

    }

    //initilize all views
    private void initilizeviews() {
        mWelcomeToqqer = (TextView) findViewById(R.id.txt_wlcm_tqqr);
        mToqermessage = (TextView) findViewById(R.id.txt_text_msg);
        Ed_CountryCode = (EditText) findViewById(R.id.input_cntry_code);
        Ed_MobileNumber = (EditText) findViewById(R.id.input_mobile);
        Ed_Country = (EditText) findViewById(R.id.input_country);
        mRegisterBtn = (Button) findViewById(R.id.btn_reg);
        mChooseLang = (TextView) findViewById(R.id.txt_choose_lng);
        mChooseLanguageImgBtn = (ImageView) findViewById(R.id.img_lng_btn);
    }

    private boolean permisions()
    {

        int permissionReadPhoneState = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int permissionreadcontacts = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (permissionReadPhoneState != PackageManager.PERMISSION_GRANTED){
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (permissionreadcontacts != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_CODE_ASK_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                // Check if the only required permission has been granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", "Contact permission has now been granted. Showing result.");
                } else {
                    permisions();
                    Log.i("Permission", "Contact permission was NOT granted.");
                }
                break;
        }
    }

    private void AppcountryCode() {
        //create dilog
        mCountryCodesDilog = new Dialog(this);
        mCountryCodesDilog.setCancelable(true);
        //add the layout of dilog
        mCountryCodesDilog.setContentView(R.layout.activity_country_list_layout);
        //find the ui of listview id
        mListviewCountryCodes = (ListView) mCountryCodesDilog.findViewById(R.id.list_of_countries);
        searchCountry = (EditText) mCountryCodesDilog.findViewById(R.id.search_Country);


        //languages create dilog
        mCountryLanguagesDilog = new Dialog(this);
        mCountryLanguagesDilog.setCancelable(true);
        //add the layout of dilog
        mCountryLanguagesDilog.setContentView(R.layout.activity_choose_language_layout);
        //find the ui of listview id
        mListviewLanguages = (ListView) mCountryLanguagesDilog.findViewById(R.id.list_of_languages);


        //item click listener
        mListviewCountryCodes.setOnItemClickListener(this);
        mListviewLanguages.setOnItemClickListener(this);

        //fetch the all countries
        CountryCodesJsonParser();

    }

    /**
     * Listview On item click listener
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getId() == R.id.list_of_countries) {
            Ed_Country.setText(countryList.get(position).getName());
            Ed_CountryCode.setText(countryList.get(position).getCode());
            searchCountry.setText("");
            mCountryCodesDilog.dismiss();
        } else if (parent.getId() == R.id.list_of_languages) {
            changeLang(mAllLanguages.get(position).getmLocale());
            mCountryLanguagesDilog.dismiss();
        }
    }

    public void loadLocale() {
        String lang = AppSharedPreferences.getInstance().getlanguage();
        if (lang != null) {
            changeLang(lang);
        }
    }

    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        AppSharedPreferences.getInstance().saveLanguage(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    private void updateTexts() {
        TextInputLayout textInputLayoutcountry = (TextInputLayout) findViewById(R.id.input_layout_country_code);
        String CountryHint = getResources().getString(R.string.action_cntry);
        textInputLayoutcountry.setHint(CountryHint);

        TextInputLayout textInputLayoutmobile = (TextInputLayout) findViewById(R.id.input_layout_mobile);
        String MobileHint = getResources().getString(R.string.action_mobile_number);
        textInputLayoutmobile.setHint(MobileHint);

        mRegisterBtn.setText(R.string.action_nxt);
        mWelcomeToqqer.setText(R.string.action_wlcm_toqqer);
        mToqermessage.setText(R.string.action_tqr_msg);
        mChooseLang.setText(R.string.action_choose_lng);
    }

    /*
   * This Method Using get the Json Array Fromat of All Country Names and Codes
   * To Parse The Data get Country Name and Code*/
    private void CountryCodesJsonParser() {
        try {
            JSONArray array = new JSONArray(CountryNames.Json);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String aCntry_nm = object.getString("name");
                String aCntry_cd = object.getString("dial_code");

                Countries list = new Countries(aCntry_nm, aCntry_cd);
                countryList.add(list);
            }
            if (countryList != null) {
                adapter = new CountryCodeAdapter(this, countryList);
                mListviewCountryCodes.setAdapter(adapter);

                Ed_Country.setText(countryList.get(0).getName());
                Ed_CountryCode.setText(countryList.get(0).getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //fetch the all languages
        CountryLanguagesJson();
    }

    private void CountryLanguagesJson() {
        try {
            JSONArray array = new JSONArray(CountryLanguages.mLanguages);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String mLanguage = object.getString("Language");
                String mLocale = object.getString("Locale");

                Languages list = new Languages(mLanguage, mLocale);
                mAllLanguages.add(list);

            }
            if (countryList != null) {
                mLngAdapter = new ChooseLangageAdapter(RegisterActivity.this, mAllLanguages);
                mListviewLanguages.setAdapter(mLngAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*It is Use to search the country name;
   * */
    class EditTextWatcher implements TextWatcher {
        private View view;

        public EditTextWatcher(View vi) {
            this.view = vi;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (adapter != null) {
                adapter.filter(s.toString());
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    //validates
    private boolean isValidates() {
        if (Ed_Country.getText().toString().isEmpty()) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please select Country", Snackbar.LENGTH_LONG)
                    .setAction("Undo", mOnClickListener);
            snackbar.setActionTextColor(Color.RED);
            View snackbarView = snackbar.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
            return false;
        }

        if (Ed_MobileNumber.getText().toString().isEmpty()) {

            String action_mobile = getResources().getString(R.string.action_enter_mobilenumber);

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), action_mobile, Snackbar.LENGTH_LONG);
            View snackbarView = snackbar.getView();
            TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
            return false;
        }
        return true;
    }

    //mobile number registration
    private void MobileNumberRegister() {
        if (isValidates()) {
            mDilog.setMessage("Please wait ....");
            showDialog();
            String mCountryCode = Ed_CountryCode.getText().toString();
            String mMobileNumber = Ed_MobileNumber.getText().toString();

            AppSharedPreferences.getInstance().setUserMobileNumber(mCountryCode+mMobileNumber);

            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    mCountryCode+mMobileNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    RegisterActivity.this, // Activity (for callback binding)
                    mCallbacks);        // OnVerificationStateChangedCallbacks

        }
    }
    //showng The Progress Dilog
    private void showDialog() {
        if (!mDilog.isShowing())
            mDilog.show();
    }
    //hide the Progress dilog
    public void hideDialog() {
        if (mDilog.isShowing())
            mDilog.dismiss();
    }

    @Override
    public void onClick(View v) {
        if (v == Ed_Country) {
            mCountryCodesDilog.show();
        } else if (v == mRegisterBtn) {

            MobileNumberRegister();
           /* JsonServer.getRegistrationService(this, this);*/
        } else if (v == mChooseLanguageImgBtn) {
            mCountryLanguagesDilog.show();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            Log.e("usersdekfjekfc",String.valueOf(user));
                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }
}
