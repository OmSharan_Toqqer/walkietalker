package com.toqqer.walkietalkie.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.backgroundservice.FetchAllDeviceContacts;
import com.toqqer.walkietalkie.utils.UUtils;
import com.toqqer.walkietalkie.database.AppSharedPreferences;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by lenovo on 9/20/2017.
 */

public class VerifyOtpScreenActivity extends AppCompatActivity implements View.OnClickListener {

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private static final String TAG = "PhoneAuthActivity";

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private TextView mVerifyTxt,mWaitingMsgTxt, mShowTopviewNumberTxt, mCountryCode_mobileNumberTxt, mWrongNumberTxt,mDigitTxt, mResendOtpBtnTxt;
    private EditText mOtpOneField, mOtpTwoField, mOtpThreeField, mOtpFourField,mOtpFiveField,mOtpSixField,
            mCurrentlyFocusedEditText;

    private String mNumber,mCode,mVerificationId;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    //Current local application:
    private Locale myLocale;
    private ProgressDialog mDilog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify_layout);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        mDilog = new ProgressDialog(VerifyOtpScreenActivity.this);
        mDilog.setCancelable(false);


        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        Bundle  bundle = getIntent().getExtras();
        if (bundle!=null){
            mNumber = bundle.getString("MOBILENUMBER");
            mCode = bundle.getString("CODE");
            mVerificationId = bundle.getString("VERIFICATIONID");
        }

        //initilize the views
        initializeUi();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //sms permissions
                requestContactPermission();
            }
        }, 1000);
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);

                hideDialog();
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.e(TAG, "onCodeSent:" + verificationId);
                hideDialog();


                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;

            }
        };


        mWrongNumberTxt.setOnClickListener(this);
        mResendOtpBtnTxt.setOnClickListener(this);
    }

    private void initializeUi(){
        mVerifyTxt = (TextView)findViewById(R.id.txt_verify);
        mWaitingMsgTxt = (TextView)findViewById(R.id.txt_waiting_text_msg);
        mShowTopviewNumberTxt = (TextView)findViewById(R.id.txt_show_number);
        mCountryCode_mobileNumberTxt = (TextView)findViewById(R.id.txt_cntry_cd_mb_num);
        mWrongNumberTxt = (TextView)findViewById(R.id.txt_wrng_num);
        mDigitTxt = (TextView)findViewById(R.id.txt_digit_num);
        mResendOtpBtnTxt = (TextView)findViewById(R.id.btn_resend);

        mOtpOneField = (EditText) findViewById(R.id.otp_one_edit_text);
        mOtpTwoField = (EditText) findViewById(R.id.otp_two_edit_text);
        mOtpThreeField = (EditText) findViewById(R.id.otp_three_edit_text);
        mOtpFourField = (EditText) findViewById(R.id.otp_four_edit_text);
        mOtpFiveField = (EditText) findViewById(R.id.otp_five_edit_text);
        mOtpSixField = (EditText) findViewById(R.id.otp_six_edit_text);

        //set the bundle data
        mShowTopviewNumberTxt.setText(mCode+" "+mNumber);
        mCountryCode_mobileNumberTxt.setText(mCode+" "+mNumber);


        //load select langage
        loadLocale();

        setFocusListener();
        setOnTextChangeListener();
    }

    public void loadLocale()
    {
        String lang = AppSharedPreferences.getInstance().getlanguage();
        if (lang!=null){
            changeLang(lang);
        }
    }
    //change the layout laguage
    public void changeLang(String lang)
    {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        AppSharedPreferences.getInstance().saveLanguage(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }
    //add the language from perticlular fields
    private void updateTexts()
    {
        mVerifyTxt.setText(R.string.action_otp_verify);
        mWaitingMsgTxt.setText(R.string.action_otp_wtng_text);
        mWrongNumberTxt.setText(R.string.action_otp_wrng_num);
        mDigitTxt.setText(R.string.action_otp_digit);
        mResendOtpBtnTxt.setText(R.string.action_otp_rsendsms);


    }
    private void requestContactPermission() {

        int hasContactPermission =ActivityCompat.checkSelfPermission(this,Manifest.permission.RECEIVE_SMS);

        if(hasContactPermission != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]   {Manifest.permission.RECEIVE_SMS}, REQUEST_CODE_ASK_PERMISSIONS);
        }else {
            //Toast.makeText(AddContactsActivity.this, "Contact Permission is already granted", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                // Check if the only required permission has been granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", "Contact permission has now been granted. Showing result.");
                } else {
                    Log.i("Permission", "Contact permission was NOT granted.");
                }
                break;
        }
    }
    /**
     * Get an instance of the present otp
     */
    private String makeOTP(){
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append(mOtpOneField.getText().toString());
        stringBuilder.append(mOtpTwoField.getText().toString());
        stringBuilder.append(mOtpThreeField.getText().toString());
        stringBuilder.append(mOtpFourField.getText().toString());
        stringBuilder.append(mOtpFiveField.getText().toString());
        stringBuilder.append(mOtpSixField.getText().toString());
        return stringBuilder.toString();
    }

    /**
     * Checks if all four fields have been filled
     * @return length of OTP
     */
    public boolean hasValidOTP(){
        return makeOTP().length()==4;
    }

    /**
     * Returns the present otp entered by the user
     * @return OTP
     */
    public String getOTP(){
        return makeOTP();
    }
    /**
     * Used to set the OTP. More of cosmetic value than functional value
     * @param otp Send the four digit otp
     */
    public void setOTP(String otp){
        if(otp.length()!=6){
            Log.e("OTPView","Invalid otp param");
            return;
        }
        if(mOtpOneField.getInputType()== InputType.TYPE_CLASS_NUMBER
                && !otp.matches("[0-9]+")){
            Log.e("OTPView","OTP doesn't match INPUT TYPE");
            return;
        }
        Log.e("sgasf", String.valueOf(otp.charAt(0)));
        Log.e("sgasf", String.valueOf(otp.charAt(1)));
        Log.e("sgasf", String.valueOf(otp.charAt(2)));
        Log.e("sgasf", String.valueOf(otp.charAt(3)));
        Log.e("sgasf", String.valueOf(otp.charAt(4)));
        Log.e("sgasf", String.valueOf(otp.charAt(5)));
        mOtpOneField.setText(String.valueOf(otp.charAt(0)));
        mOtpTwoField.setText(String.valueOf(otp.charAt(1)));
        mOtpThreeField.setText(String.valueOf(otp.charAt(2)));
        mOtpFourField.setText(String.valueOf(otp.charAt(3)));
        mOtpFiveField.setText(String.valueOf(otp.charAt(4)));
        mOtpSixField.setText(String.valueOf(otp.charAt(5)));
    }
    private void setFocusListener() {
        View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                mCurrentlyFocusedEditText = (EditText) v;
                mCurrentlyFocusedEditText.setSelection(mCurrentlyFocusedEditText.getText().length());
            }
        };
        mOtpOneField.setOnFocusChangeListener(onFocusChangeListener);
        mOtpTwoField.setOnFocusChangeListener(onFocusChangeListener);
        mOtpThreeField.setOnFocusChangeListener(onFocusChangeListener);
        mOtpFourField.setOnFocusChangeListener(onFocusChangeListener);
        mOtpFiveField.setOnFocusChangeListener(onFocusChangeListener);
        mOtpSixField.setOnFocusChangeListener(onFocusChangeListener);
    }

    private void setOnTextChangeListener() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override public void afterTextChanged(Editable s) {
                if (mCurrentlyFocusedEditText.getText().length() >= 1
                        && mCurrentlyFocusedEditText != mOtpSixField) {
                    mCurrentlyFocusedEditText.focusSearch(View.FOCUS_RIGHT).requestFocus();
                } else if (mCurrentlyFocusedEditText.getText().length() >= 1
                        && mCurrentlyFocusedEditText == mOtpSixField) {
                    mDilog.setMessage("Please wait ...");
                    showDialog();
                    //here the total all Edit fields are filled
                    UUtils.getInstance().hideKeyboard(VerifyOtpScreenActivity.this);

                    String Otp = getOTP();

                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, Otp);
                    // [END verify_with_code]
                    signInWithPhoneAuthCredential(credential);


                } else {
                    String currentValue = mCurrentlyFocusedEditText.getText().toString();
                    if (currentValue.length() <= 0 && mCurrentlyFocusedEditText.getSelectionStart() <= 0) {
                        mCurrentlyFocusedEditText.focusSearch(View.FOCUS_LEFT).requestFocus();
                    }
                }
            }
        };
        mOtpOneField.addTextChangedListener(textWatcher);
        mOtpTwoField.addTextChangedListener(textWatcher);
        mOtpThreeField.addTextChangedListener(textWatcher);
        mOtpFourField.addTextChangedListener(textWatcher);
        mOtpFiveField.addTextChangedListener(textWatcher);
        mOtpSixField.addTextChangedListener(textWatcher);
    }
    //get the otp message
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                String numberOnly= message.replaceAll("[^0-9]", "");

                Log.e("Numbers Only",numberOnly);

                setOTP(numberOnly);

                Log.e("VerifyOTP",message);

            }
        }
    };
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            hideDialog();
                            AppSharedPreferences.getInstance().setIsLocked(true);
                            Toast.makeText(VerifyOtpScreenActivity.this, "Verification success", Toast.LENGTH_SHORT).show();
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();

                            String mUserId = user.getUid();
                            if (mUserId!=null)
                                AppSharedPreferences.getInstance().setUserId(mUserId);

                            //Fetching all Contacts
                            startService(new Intent(getBaseContext(), FetchAllDeviceContacts.class));


                            Intent intent = new Intent(VerifyOtpScreenActivity.this,ProfileActivity.class);
                            startActivity(intent);
                            finish();

                            // ...
                        } else {
                            hideDialog();
                            Toast.makeText(VerifyOtpScreenActivity.this, "OTP wrong", Toast.LENGTH_SHORT).show();
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }
    //showng The Progress Dilog
    private void showDialog() {
        if (!mDilog.isShowing())
            mDilog.show();
    }
    //hide the Progress dilog
    public void hideDialog() {
        if (mDilog.isShowing())
            mDilog.dismiss();
    }


    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public void onClick(View v) {
        if (v == mWrongNumberTxt){
            Intent intent = new Intent(VerifyOtpScreenActivity.this,RegisterActivity.class);
            startActivity(intent);
        }else if (v == mResendOtpBtnTxt){
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    mCode+mNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,
                    mCallbacks,// Activity (for callback binding)
                    RegisterActivity.mResendToken);  // ForceResendingToken from callbacks
            mDilog.setMessage("Please wait ...");
            showDialog();
        }
    }
}
