package com.toqqer.walkietalkie.adapter;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.activity.ConversationActivity;
import com.toqqer.walkietalkie.activity.ConversationGroupActivity;
import com.toqqer.walkietalkie.database.DatabaseHelper;
import com.toqqer.walkietalkie.model.User;
import com.toqqer.walkietalkie.model.UserChat;
import com.toqqer.walkietalkie.ui.CircleTransform;

import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 * Created by lenovo on 6/14/2017.
 */

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {


    private ArrayList<UserChat> chatData;
    private Context context;
    private String userData;
    private FragmentManager fm;
    private GregorianCalendar calendar;


    public ChatListAdapter(ArrayList<UserChat> chatData, Context context) {
        this.chatData = chatData;
        this.context = context;
        calendar = (GregorianCalendar) GregorianCalendar.getInstance();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.people_list_card_layout, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final User user = DatabaseHelper.getInstance().getUserDetails(chatData.get(position).getReceiverUId());

        if (chatData.get(position).getmGroupname()!=null ){
            holder.user_nameTxt.setText(chatData.get(position).getmGroupname());
            holder.last_msgTxt.setText("audio file");
            holder.msg_timeTxt.setText("14:00");


        }else {
            Glide.with(context).load(user.getThumbImg())
                    .transform(new CircleTransform(context)).into(holder.user_profileImg);

            String Date = chatData.get(position).getmCurrentTime();
            String[] parserDate = Date.split(" ");


            holder.user_nameTxt.setText(user.getUserName());
            holder.last_msgTxt.setText("audio file");
            holder.msg_timeTxt.setText(parserDate[1]);

        }

      /*  holder.unread_msgTxt.setText(chatData[position].getUnreadMessage());*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (chatData.get(position).getmGroupname()!=null){
                    User user = new User();
                    user.setUserName(chatData.get(position).getmGroupname());
                    user.setUserId(chatData.get(position).getReceiverUId());
                    Intent chatRoom = new Intent(v.getContext(), ConversationGroupActivity.class);
                    chatRoom.putExtra("status", "last seen today 12:30 PM");
                    chatRoom.putExtra("user", user);
                    v.getContext().startActivity(chatRoom);
                }else {

                    Intent chatRoom = new Intent(v.getContext(), ConversationActivity.class);
                    chatRoom.putExtra("status", "last seen today 12:30 PM");
                    chatRoom.putExtra("user", user);
                    v.getContext().startActivity(chatRoom);
                }
            }
        });


        holder.rec_micBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(context, R.animator.bounce);
                holder.rec_micBtn.startAnimation(myAnim);
            }
        });
    }

    @Override
    public int getItemCount() {
        return chatData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView user_nameTxt, last_msgTxt, msg_timeTxt, unread_msgTxt;
        ImageView user_profileImg;
        ImageButton rec_micBtn;

        public MyViewHolder(View itemView) {
            super(itemView);

            user_profileImg = (ImageView) itemView.findViewById(R.id.people_profile_image);
            user_nameTxt = (TextView) itemView.findViewById(R.id.chatUserName);
            last_msgTxt = (TextView) itemView.findViewById(R.id.chatLastMsg);
            msg_timeTxt = (TextView) itemView.findViewById(R.id.chat_LastMsgTime);
            unread_msgTxt = (TextView) itemView.findViewById(R.id.chat_unreadMsg);
            rec_micBtn = (ImageButton) itemView.findViewById(R.id.chat_recBtn);

        }
    }
}
