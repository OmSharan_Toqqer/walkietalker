package com.toqqer.walkietalkie.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.activity.ConversationActivity;
import com.toqqer.walkietalkie.model.User;
import com.toqqer.walkietalkie.ui.CropSquareTransformation;

import java.util.ArrayList;

/**
 * Created by lenovo on 10/5/2017.
 */

public class ChatListUserAdapter extends RecyclerView.Adapter<ChatListUserAdapter.ViewHolder> {
    private ArrayList<User> mArrayList;
    private ArrayList<User> mFilteredList;
    private Context context;

    public ChatListUserAdapter(ArrayList<User> arrayList, Context context) {
        mArrayList = arrayList;
        mFilteredList = arrayList;
        this.context = context;
    }

    @Override
    public ChatListUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.people_chat_list_user_layout, parent, false);

        return new ChatListUserAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.user_nameTxt.setText(mFilteredList.get(position).getUserName());
        holder.user_numTxt.setText(mFilteredList.get(position).getUserContact());
        // Set image if exists
        try {

            if (mFilteredList.get(position).getThumbImg() != null) {
                Picasso.with(context)
                        .load(Uri.parse(mFilteredList.get(position).getThumbImg()))
                        .transform(new CropSquareTransformation())
                        .placeholder(R.drawable.image_holder)
                        .error(R.drawable.profile_back)
                        .into( holder.user_profileImg);
            }
        } catch (OutOfMemoryError e) {
            // Add default picture
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {
        TextView user_nameTxt, user_numTxt;
        ImageView user_profileImg;

        public ViewHolder(View itemView) {
            super(itemView);

            user_profileImg = (ImageView) itemView.findViewById(R.id.profile_image);
            user_nameTxt = (TextView) itemView.findViewById(R.id.UserName);
            user_numTxt = (TextView) itemView.findViewById(R.id.chatUserNumber);


        }
    }
}
