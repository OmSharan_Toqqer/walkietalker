package com.toqqer.walkietalkie.adapter;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.model.Chat;
import com.toqqer.walkietalkie.model.ChatMessages;
import com.toqqer.walkietalkie.model.UserChat;
import com.toqqer.walkietalkie.utils.UUtils;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by lenovo on 10/9/2017.
 */

public class ChatMessagesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContxt;
    private static final int VIEW_TYPE_ME = 1;
    private static final int VIEW_TYPE_OTHER = 2;
    private ArrayList<UserChat> chatMessages;
    private Handler mHandler = new Handler();
    public ChatMessagesRecyclerAdapter(ArrayList<UserChat> messages, Context context){
        this.chatMessages = messages;
        this.mContxt = context;
    }
    public void add(UserChat chat) {
        chatMessages.add(chat);
        notifyItemInserted(chatMessages.size() - 1);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_ME:
                View viewChatMine = layoutInflater.inflate(R.layout.chat_from_me_layout, parent, false);
                viewHolder = new MyChatViewHolder(viewChatMine);
                break;
            case VIEW_TYPE_OTHER:
                View viewChatOther = layoutInflater.inflate(R.layout.chat_from_other_layout, parent, false);
                viewHolder = new OtherChatViewHolder(viewChatOther);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (TextUtils.equals(chatMessages.get(position).getSenderUId(),
                FirebaseAuth.getInstance().getCurrentUser().getUid())) {

            configureMyChatViewHolder((MyChatViewHolder) holder, position);

           /* MyChatViewHolder chatViewHolder = (MyChatViewHolder)holder;

            Log.e("Url",messages.message);

            chatViewHolder.txtStartTime.setText(messages.sender);*/

        } else {
            configureOtherChatViewHolder((OtherChatViewHolder) holder, position);
           /* OtherChatViewHolder otherChatViewHolder = (OtherChatViewHolder)holder;
            Log.e("Url",messages.message);
            otherChatViewHolder.txtStartTime1.setText(messages.receiver);*/
        }

    }

    private void configureMyChatViewHolder(final MyChatViewHolder holder, final int position) {
        final MediaPlayer mPlayer = new MediaPlayer();
        // Play song
        try {
            mPlayer.reset();
            mPlayer.setDataSource(chatMessages.get(position).getMessage());
            mPlayer.prepare();
            holder.txtEndTime.setText(""+ UUtils.milliSecondsToTimer(mPlayer.getDuration()));

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Handler to update UI timer, progress bar etc,.


        holder.mPlayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runnable mUpdateTimeTask = new Runnable() {
                    public void run() {
                        long totalDuration = mPlayer.getDuration();
                        final long currentDuration = mPlayer.getCurrentPosition();

                        // Displaying time completed playing
                        holder.txtStartTime.setText(""+UUtils.milliSecondsToTimer(currentDuration));

                        // Updating progress bar
                        int progress = (int)(UUtils.getProgressPercentage(currentDuration, totalDuration));
                        //Log.d("Progress", ""+progress);
                        holder.mSeekBar.setProgress(progress);

//                        mPlayer.reset();
//                        holder.mPlayBtn.setImageResource(R.drawable.play_music);

                        // Running this thread after 100 milliseconds
                        mHandler.postDelayed(this, 100);
                    }
                };
                mHandler.postDelayed(mUpdateTimeTask, 100);

                if (mPlayer.isPlaying()) {
                    if (mPlayer != null) {
                        mPlayer.pause();
                        // Changing button image to play button
                        holder.mPlayBtn.setImageResource(R.drawable.play_music);
                    }
                } else {
                    // Resume song
                    if (mPlayer != null) {
                        mPlayer.start();
                        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {

                                holder.mPlayBtn.setImageResource(R.drawable.play_music);

                            }
                        });
                        // Changing button image to pause button
                        holder.mPlayBtn.setImageResource(R.drawable.pause_music);
                    }

                }
            }
        });

    }

    private void configureOtherChatViewHolder(final OtherChatViewHolder holder, final int position) {

        final MediaPlayer oPlayer = new MediaPlayer();
        // Play song
        try {
            oPlayer.reset();
            oPlayer.setDataSource(chatMessages.get(position).getMessage());
            oPlayer.prepare();

            holder.txtEndTime1.setText(""+ UUtils.milliSecondsToTimer(oPlayer.getDuration()));

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Handler to update UI timer, progress bar etc,.

        holder.mPlayBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runnable mUpdateTimeTask = new Runnable() {
                    public void run() {
                        long totalDuration = oPlayer.getDuration();
                        final long currentDuration = oPlayer.getCurrentPosition();

                        // Displaying time completed playing
                        holder.txtStartTime1.setText(""+UUtils.milliSecondsToTimer(currentDuration));

                        // Updating progress bar
                        int progress = (int)(UUtils.getProgressPercentage(currentDuration, totalDuration));
                        //Log.d("Progress", ""+progress);
                        holder.mSeekBar1.setProgress(progress);

//                        oPlayer.reset();
//                        holder.mPlayBtn1.setImageResource(R.drawable.play_music);
                        // Running this thread after 100 milliseconds
                        mHandler.postDelayed(this, 100);
                    }
                };
                mHandler.postDelayed(mUpdateTimeTask, 100);

                // check for already playing
                if (oPlayer.isPlaying()) {
                    if (oPlayer != null) {
                        oPlayer.pause();
                        // Changing button image to play button
                        holder.mPlayBtn1.setImageResource(R.drawable.play_music);
                    }

                } else {
                    // Resume song
                    if (oPlayer != null) {
                        oPlayer.start();
                        oPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {

                                holder.mPlayBtn1.setImageResource(R.drawable.play_music);
//                                holder.mSeekBar1.setProgress(0);
                            }
                        });
                        // Changing button image to pause button
                        holder.mPlayBtn1.setImageResource(R.drawable.pause_music);
                    }
                }
            }
        });

    }
    @Override
    public int getItemCount() {
        if (chatMessages != null) {
            return chatMessages.size();
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (TextUtils.equals(chatMessages.get(position).getSenderUId(),
                FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            return VIEW_TYPE_ME;
        } else {
            return VIEW_TYPE_OTHER;
        }
    }

    public class MyChatViewHolder extends RecyclerView.ViewHolder  {
        private TextView txtStartTime, txtEndTime;
        private ImageView mPlayBtn;
        private SeekBar mSeekBar;

        public MyChatViewHolder(View itemView) {
            super(itemView);
            mPlayBtn = (ImageView)itemView.findViewById(R.id.play_btn);
            txtStartTime = (TextView) itemView.findViewById(R.id.start_time_txt);
            txtEndTime = (TextView) itemView.findViewById(R.id.end_time_txt);
            mSeekBar = (SeekBar)itemView.findViewById(R.id.seek_bar);
        }

    }
    public class OtherChatViewHolder extends RecyclerView.ViewHolder  {
        private TextView txtStartTime1, txtEndTime1;
        private ImageView mPlayBtn1;
        private SeekBar mSeekBar1;

        public OtherChatViewHolder(View itemView) {
            super(itemView);
            mPlayBtn1 = (ImageView)itemView.findViewById(R.id.ot_play_btn);
            txtStartTime1 = (TextView) itemView.findViewById(R.id.ot_start_time_txt);
            txtEndTime1 = (TextView) itemView.findViewById(R.id.ot_end_time_txt);
            mSeekBar1 = (SeekBar)itemView.findViewById(R.id.ot_seek_bar);
        }

    }
}
