package com.toqqer.walkietalkie.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.model.Languages;

import java.util.ArrayList;

/**
 * Created by lenovo on 9/25/2017.
 */

public class ChooseLangageAdapter extends BaseAdapter {
    private Context mCon;
    private ArrayList<Languages>mLngList;
    public ChooseLangageAdapter(Context context, ArrayList<Languages> languagesArrayList){
        this.mCon = context;
        this.mLngList = languagesArrayList;
    }
    @Override
    public int getCount() {
        return mLngList.size();
    }

    @Override
    public Object getItem(int position) {
        return mLngList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater li = (LayoutInflater) mCon.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.language_list_adapter_layout, null);
        } else {
            view = convertView;
        }

        TextView mLngtxt = (TextView) view.findViewById(R.id.txt_lng_text);

        mLngtxt.setText(mLngList.get(position).getmLanguage());

        return view;
    }
}
