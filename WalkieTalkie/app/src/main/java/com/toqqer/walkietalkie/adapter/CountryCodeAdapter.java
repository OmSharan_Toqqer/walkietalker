package com.toqqer.walkietalkie.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.model.Countries;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by lenovo on 9/19/2017.
 */

public class CountryCodeAdapter extends BaseAdapter {
    Context con;
    public ArrayList<Countries> countriesList;
    public ArrayList<Countries> arraylist;
    public CountryCodeAdapter(Context context, ArrayList<Countries> texts) {
        this.con = context;
        this.countriesList = texts;
        arraylist = new ArrayList<>();
        arraylist.addAll(texts);
    }
    @Override
    public int getCount() {
        return countriesList.size();
    }

    @Override
    public Object getItem(int i) {
        return countriesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View view = convertView;

        if (view == null) {
            LayoutInflater li = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.country_code_adapter_layout, null);
        } else {
            view = convertView;
        }

        TextView Country_Name = (TextView) view.findViewById(R.id.txt_cntry_nm);
        TextView Country_Code = (TextView) view.findViewById(R.id.txt_cntry_cd);

        Country_Name.setText(countriesList.get(i).getName());
        Country_Code.setText(countriesList.get(i).getCode());

        return view;
    }
    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        countriesList.clear();
        if (charText.length() == 0) {
            countriesList.addAll(this.arraylist);
            notifyDataSetChanged();
        } else {
            for (Countries wp : this.arraylist) {
                if (wp != null) {

                    if (wp.getName() != null && wp.getName().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        countriesList.add(wp);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }
}
