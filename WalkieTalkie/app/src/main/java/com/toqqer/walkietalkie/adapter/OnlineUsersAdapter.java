package com.toqqer.walkietalkie.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.activity.ConversationActivity;
import com.toqqer.walkietalkie.model.OnlineUser;

import java.util.ArrayList;

/**
 * Created by lenovo on 9/25/2017.
 */

public class OnlineUsersAdapter extends BaseAdapter {
    private Context mCon;
    private ArrayList<OnlineUser> mUserList;
    public OnlineUsersAdapter(Context context, ArrayList<OnlineUser> onlineUserArrayList){
        this.mCon = context;
        this.mUserList = onlineUserArrayList;
    }
    @Override
    public int getCount() {
        return mUserList.size();
    }

    @Override
    public Object getItem(int position) {
        return mUserList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return mUserList.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mCon.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.online_user_layout, null, false);

            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.mOnlineUserProfile.setImageResource(mUserList.get(position).getImageSource());
        viewHolder.mOnlineUserName.setText(mUserList.get(position).getName());

        return convertView;
    }
    private static class ViewHolder {
        private TextView mOnlineUserName;
        private ImageView mOnlineUserProfile;

        public ViewHolder(View v) {
            mOnlineUserProfile = (ImageView) v.findViewById(R.id.people_profile_image);
            mOnlineUserName = (TextView) v.findViewById(R.id.txt_pepl_nm);



        }
    }
}
