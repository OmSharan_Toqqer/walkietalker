package com.toqqer.walkietalkie.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.model.User;
import com.toqqer.walkietalkie.ui.CircleTransform;
import com.toqqer.walkietalkie.ui.CropSquareTransformation;

import java.util.ArrayList;

/**
 * Created by lenovo on 10/11/2017.
 */

public class UserListAdapter extends BaseAdapter implements Filterable{
    private ArrayList<User> mArrayList;
    private ArrayList<User> mFilteredList;
    private Context context;
    public UserListAdapter(ArrayList<User> arrayList, Context context){
        mArrayList = arrayList;
        mFilteredList = arrayList;
        this.context = context;

    }
    @Override
    public int getCount() {
        return mFilteredList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilteredList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.people_chat_list_user_layout,null);
        }else {
            view =convertView;
        }

        ImageView user_profileImg = (ImageView)view.findViewById(R.id.profile_image);
        TextView user_nameTxt = (TextView)view.findViewById(R.id.UserName);
        TextView user_numTxt = (TextView)view.findViewById(R.id.chatUserNumber);
        ImageView mSelectIcon = (ImageView)view.findViewById(R.id.addIcon);

        user_nameTxt.setText(mFilteredList.get(position).getUserName());
        user_numTxt.setText(mFilteredList.get(position).getUserContact());

        if (mFilteredList.get(position).getThumbImg()!=null) {

            Glide.with(context).load(mFilteredList.get(position).getThumbImg())
                    .transform(new CircleTransform(context)).into(user_profileImg);


            /*Picasso.with(context)
                    .load(mFilteredList.get(position).getThumbImg())
                    .transform(new CropSquareTransformation())
                    .placeholder(R.drawable.image_holder)
                    .error(R.drawable.profile_back)
                    .into(user_profileImg);*/
        }
        if (mFilteredList.get(position).getIsGroupValue() == 1){
            mSelectIcon.setVisibility(View.VISIBLE);
        }else {
            mSelectIcon.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<User> filteredList = new ArrayList<>();

                    for (User androidVersion : mArrayList) {

                        if (androidVersion.getUserName().toLowerCase().contains(charString) || androidVersion.getUserContact().toLowerCase().contains(charString) ) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<User>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
