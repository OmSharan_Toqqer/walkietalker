package com.toqqer.walkietalkie.application;

import android.app.Application;
import android.content.Context;

import com.toqqer.walkietalkie.database.AppSharedPreferences;
import com.toqqer.walkietalkie.database.DatabaseHelper;


/**
 * Created by praveen on 3/16/2017.
 */

public class MainApplication extends Application {

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);

    }
    @Override
    public void onCreate() {
        super.onCreate();

        //initilize sharedprefference
        AppSharedPreferences.getInstance().Initialize(getApplicationContext());

        //initilize database
        DatabaseHelper.init(getApplicationContext());

    }



}
