package com.toqqer.walkietalkie.backgroundservice;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.text.TextUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.toqqer.walkietalkie.database.AppSharedPreferences;
import com.toqqer.walkietalkie.database.DatabaseHelper;
import com.toqqer.walkietalkie.model.Upload;
import com.toqqer.walkietalkie.model.User;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by lenovo on 10/4/2017.
 */

public class FetchAllDeviceContacts extends Service {
    private ContentResolver cr;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        cr =getContentResolver();
        getContactNames();

    }
    private void  getContactNames() {

        // Get the Cursor of all the contacts
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE ASC");

        // Move the cursor to first. Also check whether the cursor is empty or not.
        if (cursor.moveToFirst()) {
            // Iterate through the cursor
            do {
                // Get the contacts number
                String phone_number=cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
               /* String Photo_Thumb = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));*/
                String Name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                String phone_spaces=phone_number;

                if (phone_spaces.contains("-")){
                    phone_spaces=phone_spaces.replace("-"," ");
                }
                if (phone_spaces.contains("(")){
                    phone_spaces=phone_spaces.replace("(","");
                }
                if (phone_spaces.contains(")")){
                    phone_spaces=phone_spaces.replace(")","");
                }
                if (phone_spaces.contains(" ")){
                    phone_spaces=phone_spaces.replace(" ","");
                }
                if (phone_spaces.contains("+91")){
                }else {
                    phone_spaces = "+91"+phone_spaces;
                }
                if (phone_spaces.contains("*")){
                    phone_spaces=phone_spaces.replace("*","");
                }
                if (phone_spaces.contains("#")){
                    phone_spaces=phone_spaces.replace("#","");
                }

                final User user = new User();
                user.setUserName(Name);
                user.setUserContact(phone_spaces);

                FirebaseDatabase.getInstance().getReference().child("users").child(phone_spaces+"/user").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue()!=null && dataSnapshot.getValue()!="123") {

                            Upload upload = dataSnapshot.getValue(Upload.class);
                            user.setUser_firebase_token(upload.getFirebaseid());
                            user.setUserId(upload.getUserId());
                            user.setThumbImg(upload.getUrl());
                            User dataUser = DatabaseHelper.getInstance().getUserDetails(upload.getUserId());
                            if (dataUser.getUserId()!=null && !dataUser.getUserId().isEmpty()) {
                                DatabaseHelper.getInstance().UpdateUserDetails(user,Integer.parseInt(dataUser.getDbId()));
                            } else {
                                if (!TextUtils.equals(AppSharedPreferences.getInstance().getUserId(),upload.getUserId()))
                                    DatabaseHelper.getInstance().insertAllUsers(user);

                            }
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            } while (cursor.moveToNext());
        }
        // Close the curosor
        cursor.close();
        stopSelf();
    }

}
