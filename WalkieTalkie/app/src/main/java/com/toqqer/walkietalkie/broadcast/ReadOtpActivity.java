package com.toqqer.walkietalkie.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by lenovo on 10/4/2017.
 */

public class ReadOtpActivity extends BroadcastReceiver {

    private static final String TAG = "ReadOtpActivity";

    @Override
    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);

                    String sender = smsMessage.getDisplayOriginatingAddress();
                    //You must check here if the sender is your provider and not another one with same text.

                    String messageBody = smsMessage.getMessageBody();

                    Intent myIntent = new Intent("otp");
                    myIntent.putExtra("message", messageBody);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(myIntent);

                }
            }

        } catch (Exception e) {
            Log.e(TAG, "Exception smsReceiver" +e);

        }
    }
}
