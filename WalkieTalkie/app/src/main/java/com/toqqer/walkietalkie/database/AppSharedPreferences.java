package com.toqqer.walkietalkie.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by praveen on 5/19/2017.
 */

public class AppSharedPreferences
{
    private static AppSharedPreferences mInstance;
    private Context mContext;

    private SharedPreferences mMyPreferences;

    private AppSharedPreferences(){ }

    public static AppSharedPreferences getInstance(){
        if (mInstance == null) {
            mInstance = new AppSharedPreferences();
        }
        return mInstance;
    }

    public void Initialize(Context ctxt){
        mContext = ctxt;
        mMyPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }


    public void saveLanguage(String value){
        SharedPreferences.Editor e = mMyPreferences.edit();
        e.putString("Language", value);
        e.commit();
    }

    public String getlanguage(){
        return mMyPreferences.getString("Language", "");
    }

    public void saveFirebaseId(String value){
        SharedPreferences.Editor e = mMyPreferences.edit();
        e.putString("FIREBASE", value);
        e.commit();
    }
    public String getFirebaseId(){
        return mMyPreferences.getString("FIREBASE", "");
    }
    public void setIsLocked(boolean locked){
        SharedPreferences.Editor e = mMyPreferences.edit();
        e.putBoolean("ISLOCKED", locked);
        e.commit();
    }
    public boolean getIsLocked(){
        return mMyPreferences.getBoolean("ISLOCKED",false);
    }

    public void setUserMobileNumber(String number){
        SharedPreferences.Editor e = mMyPreferences.edit();
        e.putString("user", number);
        e.commit();
    }
    public String getUserMobileNumber(){
        return mMyPreferences.getString("user", "");
    }
    public void setUserId(String UserId){
        SharedPreferences.Editor e = mMyPreferences.edit();
        e.putString("user_Id", UserId);
        e.commit();
    }
    public String getUserId(){
        return mMyPreferences.getString("user_Id", "");
    }

}


