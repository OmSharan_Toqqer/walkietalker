package com.toqqer.walkietalkie.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.toqqer.walkietalkie.Constants.Constants;
import com.toqqer.walkietalkie.model.getgroupdetails;
import com.toqqer.walkietalkie.model.User;
import com.toqqer.walkietalkie.model.UserChat;

import java.util.ArrayList;

import static com.toqqer.walkietalkie.Constants.Constants.WT_All_USERS;
import static com.toqqer.walkietalkie.Constants.Constants.WT_CHILD_GROUP_USERS;
import static com.toqqer.walkietalkie.Constants.Constants.WT_DUMMY_USER_MESSAGES;
import static com.toqqer.walkietalkie.Constants.Constants.WT_GROUP_MESSAGES;
import static com.toqqer.walkietalkie.Constants.Constants.WT_PARENT_GROUPS;
import static com.toqqer.walkietalkie.Constants.Constants.WT_PROFILE_USER;
import static com.toqqer.walkietalkie.Constants.Constants.WT_USER_MESSAGES;

/**
 * Created by lenovo on 10/4/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Walkie_Talkie";

    private static final int DATABASE_VERSION = 1;

    private static DatabaseHelper mInstance = null;
    /**
     * Initializes the database for the first time.
     *
     * @param context Instance of context.
     */
    public static synchronized void init(Context context) {
        // If the instance is null then initialize the instance.
        if (mInstance == null) {
            mInstance = new DatabaseHelper(context);

        }
    }
    public static DatabaseHelper getInstance() {
        return mInstance;
    }
    /**
     * Constructor should be private to prevent direct instantiation.
     * Make a call to the static method "getInstance()" instead.
     */
    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    // Called when the database is created for the FIRST time.
    // If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_PROFILE_USER_TABLE = "create table if not exists " + WT_PROFILE_USER + "("
                + Constants.ID + " integer primary key,"
                + Constants.PF_USER_ID + " integer,"
                + Constants.PF_USER_NAME + " text,"
                + Constants.PF_USER_IMAGE + " text)";

        String CREATE_ALL_USERS_TABLE = "create table if not exists "+ WT_All_USERS + "("
                + Constants.ID + " integer primary key,"
                + Constants.AU_USER_ID + " text,"
                + Constants.AU_USER_FIREBASE_TOKEN + " text,"
                + Constants.AU_USER_NAME + " text,"
                + Constants.AU_CONTACT + " text,"
                + Constants.AU_USER_IMAGE + " text)";

        String CREATE_ALL_USERS_MESSAGES = "create table if not exists "+ WT_USER_MESSAGES + "("
                + Constants.ID + " integer primary key,"
                + Constants.AUM_USER_ID + " text,"
                + Constants.AUM_RECEIVER_ID + " text,"
                + Constants.AUM_MESSAGE + " text,"
                + Constants.AUM_MESSAGE_ID + " integer,"
                + Constants.AUM_MESSAGE_TEXT + " text,"
                + Constants.AUM_CURRENT_TIME + " text,"
                + Constants.AUM_STATUS + " integer)";
        String CREATE_DUMMY_ALL_USERS_MESSAGES = "create table if not exists "+ WT_DUMMY_USER_MESSAGES + "("
                + Constants.ID + " integer primary key,"
                + Constants.AUM_USER_ID + " text,"
                + Constants.AUM_RECEIVER_ID + " text,"
                + Constants.AUM_MESSAGE + " text,"
                + Constants.AUM_MESSAGE_ID + " integer,"
                + Constants.AUM_MESSAGE_TEXT + " text,"
                + Constants.AUM_CURRENT_TIME + " integer,"
                + Constants.AUM_STATUS + " integer)";

        String CREATE_PARENT_GROUP_TABLE = "create table if not exists "+ WT_PARENT_GROUPS + "("
                + Constants.PG_ID + " integer primary key,"
                + Constants.PG_NAME + " text,"
                + Constants.PG_IMAGE + " text)";

        String CREATE_CHILD_GROUP_TABLE = "create table if not exists "+ WT_CHILD_GROUP_USERS + "("
                + Constants.CG_ID + " text,"
                + Constants.CG_USER_ID + " text)";

        String CREATE_GROUP_MESSAGE_TABLE = "create table if not exists "+ WT_GROUP_MESSAGES + "("
                + Constants.ID + " integer primary key,"
                + Constants.G_ID + " text,"
                + Constants.G_USER_ID + " text,"
                + Constants.G_MESSAGE + " text,"
                + Constants.G_MESSAGE_ID + " integer,"
                + Constants.G_MESSAGE_TEXT + "text,"
                + Constants.G_TIME_STAMP + " text)";


        String[] quaries = {CREATE_PROFILE_USER_TABLE,CREATE_ALL_USERS_TABLE,CREATE_ALL_USERS_MESSAGES,CREATE_DUMMY_ALL_USERS_MESSAGES,CREATE_PARENT_GROUP_TABLE,
                CREATE_CHILD_GROUP_TABLE,CREATE_GROUP_MESSAGE_TABLE};

        // Iterating through each query in the array and executing them.
        for (String query : quaries) {
            db.execSQL(query);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table if exists " + WT_PROFILE_USER);
        db.execSQL("Drop table if exists " + WT_All_USERS);
        db.execSQL("Drop table if exists " + WT_USER_MESSAGES);
        db.execSQL("Drop table if exists " + WT_PARENT_GROUPS);
        db.execSQL("Drop table if exists " + WT_CHILD_GROUP_USERS);
        db.execSQL("Drop table if exists " + WT_GROUP_MESSAGES);
    }
    /************************************************* Profile Table Details ****************************************************************/
    //profile details
    public void insert_user_profile_details(){
        ContentValues values = new ContentValues();
        values.put(Constants.PF_USER_ID,"");
        values.put(Constants.PF_USER_NAME,"");
        values.put(Constants.PF_USER_IMAGE,"");

    }

    /**************************************************** All Users Table Details ********************************************************/

    //all users
    public void insertAllUsers(User users){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.AU_USER_ID,users.getUserId());
        values.put(Constants.AU_USER_FIREBASE_TOKEN,users.getUser_firebase_token());
        values.put(Constants.AU_USER_NAME,users.getUserName());
        values.put(Constants.AU_USER_IMAGE,users.getThumbImg());
        values.put(Constants.AU_CONTACT,users.getUserContact());


        long id = db.insert(WT_All_USERS,null,values);

        Log.e("AllUsers",String.valueOf(id));

    }

    /** Returns all the peoples in the table */
    public Cursor getAllUsers() throws SQLiteException
    {
        SQLiteDatabase db=this.getReadableDatabase();
        return db.query(WT_All_USERS, new String[] {Constants.ID,Constants.AU_USER_ID,Constants.AU_USER_FIREBASE_TOKEN,Constants.AU_USER_NAME,Constants.AU_CONTACT,Constants.AU_USER_IMAGE} ,
                null, null, null, null,
                Constants.AU_USER_NAME + " asc ");
    }
    //validate if user id saved or not
    public boolean validateUser(String userid) throws SQLiteException{
        Cursor c = this.getReadableDatabase().rawQuery(
                "SELECT * FROM " + WT_All_USERS + " WHERE "
                        + Constants.AU_USER_ID + "='" + userid +"'",  null);
        if (c.getCount()>0)
            return true;
        return false;
    }
    //get User Name
    public String getUsername(String ReceiverId){
        String username =null;
        Cursor c = this.getReadableDatabase().rawQuery(
                "SELECT * FROM " + WT_All_USERS + " WHERE "
                        + Constants.AU_USER_ID + "='" + ReceiverId +"'",  null);
        if (c.getCount()>0 && c.moveToFirst()){
             username=c.getString(c.getColumnIndex(Constants.AU_USER_NAME));
        }

        return username;
    }
    public User getUserDetails(String ReceiverId){
        User user = new User();
        Cursor c = this.getReadableDatabase().rawQuery(
                "SELECT * FROM " + WT_All_USERS + " WHERE "
                        + Constants.AU_USER_ID + "='" + ReceiverId +"'",  null);
        if (c.getCount()>0 && c.moveToFirst()){
           user = getUser(c);
        }
        return user;
    }
    public User getUser(Cursor cursor){
        User user = new User(
                cursor.getString(cursor.getColumnIndex(Constants.AU_USER_ID)),
                cursor.getString(cursor.getColumnIndex(Constants.AU_USER_FIREBASE_TOKEN)),
                cursor.getString(cursor.getColumnIndex(Constants.AU_USER_IMAGE)),
                cursor.getString(cursor.getColumnIndex(Constants.AU_CONTACT)),
                cursor.getString(cursor.getColumnIndex(Constants.AU_USER_NAME)));
        user.setDbId(cursor.getString(cursor.getColumnIndex(Constants.ID)));
        return user;
    }
    public void UpdateUserDetails(User users,int id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.AU_USER_ID,users.getUserId());
        values.put(Constants.AU_USER_FIREBASE_TOKEN,users.getUser_firebase_token());
        values.put(Constants.AU_USER_NAME,users.getUserName());
        values.put(Constants.AU_USER_IMAGE,users.getThumbImg());
        values.put(Constants.AU_CONTACT,users.getUserContact());

        String[] ids=new String[]{ id +""};
        db.update(WT_All_USERS,values,Constants.ID + " = ? ",ids);
        db.close();

        Log.e("DATABASEOPERATION","Update Sending Response Row ............");
    }

    /**************************************************** Chat Messages Tables Details ***************************************************/

    //insert the user messages
    public void insert_Chatmessages(UserChat userChat){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.AUM_USER_ID,userChat.getSenderUId());
        values.put(Constants.AUM_RECEIVER_ID,userChat.getReceiverUId());
        values.put(Constants.AUM_MESSAGE,userChat.getMessage());
        values.put(Constants.AUM_MESSAGE_ID,userChat.getMessageId());
        values.put(Constants.AUM_MESSAGE_TEXT,userChat.getMessageTxt());
        values.put(Constants.AUM_CURRENT_TIME,userChat.getmCurrentTime());
        values.put(Constants.AUM_STATUS,userChat.getMstatus());


        long id = db.insert(WT_USER_MESSAGES,null,values);

        Log.e("chat message",String.valueOf(id));
    }
    //insert the dummy user messages
    public void insert_Dummy_Chatmessages(UserChat userChat){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.AUM_USER_ID,userChat.getSenderUId());
        values.put(Constants.AUM_RECEIVER_ID,userChat.getReceiverUId());
        values.put(Constants.AUM_MESSAGE,userChat.getMessage());
        values.put(Constants.AUM_MESSAGE_ID,userChat.getMessageId());
        values.put(Constants.AUM_MESSAGE_TEXT,userChat.getMessageTxt());
        values.put(Constants.AUM_CURRENT_TIME,userChat.getmCurrentTime());
        values.put(Constants.AUM_STATUS,userChat.getMstatus());


        long id = db.insert(WT_DUMMY_USER_MESSAGES,null,values);

        Log.e("dummy chat message",String.valueOf(id));
    }
    /** Returns all the chatlist people in the table */
    public Cursor getAllChatUsers() throws SQLiteException
    {
        SQLiteDatabase db=this.getReadableDatabase();
        return db.query(WT_DUMMY_USER_MESSAGES, new String[] {Constants.AUM_USER_ID,Constants.AUM_RECEIVER_ID,Constants.AUM_MESSAGE,
                        Constants.AUM_MESSAGE_ID,Constants.AUM_MESSAGE_TEXT,Constants.AUM_CURRENT_TIME,Constants.AUM_STATUS} ,
                null, null, null, null,null);
    }
    //validate if receiver id saved or not
    public boolean isValideUserMessage(String receiverId) throws SQLiteException{
        Cursor c = this.getReadableDatabase().rawQuery(
                "SELECT * FROM " + WT_DUMMY_USER_MESSAGES + " WHERE "
                        + Constants.AUM_RECEIVER_ID + "='" + receiverId +"'",  null);
        if (c.getCount()>0)
            return true;
        return false;
    }
    //validate if user timestamp id saved or not
    public boolean isValideUsermessageTimestamp(String timestamp) throws SQLiteException{
        Cursor c = this.getReadableDatabase().rawQuery(
                "SELECT * FROM " + WT_USER_MESSAGES + " WHERE "
                        + Constants.AUM_CURRENT_TIME + "='" + timestamp +"'",  null);
        if (c.getCount()>0)
            return true;
        return false;
    }

    //---deletes a particular ReceiverId---
    public boolean deletePreviousMessage(String receiverId)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.delete(WT_DUMMY_USER_MESSAGES, Constants.AUM_RECEIVER_ID + "=" +receiverId, null) > 0;
    }
    public ArrayList<UserChat> getAllMessages(String UserId,String ReceiverId){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<UserChat> returnList = new ArrayList<>();
        String[] selectionArgs = new String[]{UserId,ReceiverId};

        Cursor mCur = db.rawQuery("select * from "+ WT_USER_MESSAGES +" where "+ Constants.AUM_USER_ID +"=? and "+ Constants.AUM_RECEIVER_ID +"=?", selectionArgs);
        if (mCur.moveToFirst()){
            do {
                UserChat model = getUserChatMessages(mCur);
                returnList.add(model);
            }while (mCur.moveToNext());
        }
        mCur.close();
        db.close();
        return returnList;
    }

    public UserChat getUserChatMessages(Cursor cursor){
        UserChat chat = new UserChat(
                cursor.getString(cursor.getColumnIndex(Constants.AUM_USER_ID)),
                cursor.getString(cursor.getColumnIndex(Constants.AUM_RECEIVER_ID)),
                cursor.getString(cursor.getColumnIndex(Constants.AUM_MESSAGE)),
                cursor.getInt(cursor.getColumnIndex(Constants.AUM_MESSAGE_ID)),
                cursor.getString(cursor.getColumnIndex(Constants.AUM_MESSAGE_TEXT)),
                cursor.getString(cursor.getColumnIndex(Constants.AUM_CURRENT_TIME)),
                cursor.getInt(cursor.getColumnIndex(Constants.AUM_STATUS))
        );
        return  chat;
    }



    /************************************************* getgroupdetails Tables Details ****************************************************************/

    public void insert_parentGroup(getgroupdetails getgroupdetails){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.PG_NAME, getgroupdetails.getmGroupName());
        values.put(Constants.PG_IMAGE, getgroupdetails.getmGroupicon());


        long id = db.insert(WT_PARENT_GROUPS,null,values);

        Log.e("Parentgroup",String.valueOf(id));
    }
    public void insert_childgroup(getgroupdetails getgroupdetails){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.CG_ID, getgroupdetails.getmGroupId());
        values.put(Constants.CG_USER_ID, getgroupdetails.getmUserIds());


        long id = db.insert(WT_CHILD_GROUP_USERS,null,values);

        Log.e("Childgroup",String.valueOf(id));
    }
    public ArrayList<getgroupdetails> getGroupName(String name){
        getgroupdetails bannerModels = new getgroupdetails();
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<getgroupdetails> modelses = new ArrayList<>();

        Cursor mCur = db.query(WT_PARENT_GROUPS,null,Constants.PG_NAME + " =?",new String[]{name},null,null,null);
        if (mCur.moveToFirst()){
            do {
                bannerModels = getGroupDetails(mCur);
                modelses.add(bannerModels);
            }while (mCur.moveToNext());
        }
        mCur.close();
        db.close();
        return modelses;
    }
    public void insert_group_messages(UserChat userChat){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.G_ID,userChat.getReceiverUId());
        values.put(Constants.G_USER_ID,userChat.getSenderUId());
        values.put(Constants.G_MESSAGE,userChat.getMessage());
        values.put(Constants.G_MESSAGE_ID,userChat.getMessageId());
        values.put(Constants.G_MESSAGE_TEXT,userChat.getMessageTxt());
        values.put(Constants.G_TIME_STAMP,userChat.getMstatus());


        long id = db.insert(WT_GROUP_MESSAGES,null,values);

        Log.e("chat message",String.valueOf(id));
    }
    //validate if user timestamp id saved or not
    public boolean isValideGroupmessageTimestamp(String timestamp) throws SQLiteException{
        Cursor c = this.getReadableDatabase().rawQuery(
                "SELECT * FROM " + WT_GROUP_MESSAGES + " WHERE "
                        + Constants.G_TIME_STAMP + "='" + timestamp +"'",  null);
        if (c.getCount()>0)
            return true;
        return false;
    }
    public ArrayList<UserChat> getGroupAllMessages(String groupId){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<UserChat> returnList = new ArrayList<>();

        Cursor mCur = db.query(WT_GROUP_MESSAGES,null,Constants.G_ID + " =?",new String[]{groupId},null,null,null);
        if (mCur.moveToFirst()){
            do {
                UserChat model = getGroupChatMessages(mCur);
                returnList.add(model);
            }while (mCur.moveToNext());
        }
        mCur.close();
        db.close();
        return returnList;
    }

    public UserChat getGroupChatMessages(Cursor cursor){
        UserChat chat = new UserChat(
                cursor.getString(cursor.getColumnIndex(Constants.G_USER_ID)),
                cursor.getString(cursor.getColumnIndex(Constants.G_ID)),
                cursor.getString(cursor.getColumnIndex(Constants.G_MESSAGE)),
                cursor.getInt(cursor.getColumnIndex(Constants.G_MESSAGE_ID)),
                cursor.getString(cursor.getColumnIndex(Constants.G_MESSAGE_TEXT)),
                cursor.getString(cursor.getColumnIndex(Constants.G_TIME_STAMP)),0);
        return  chat;
    }
    /**
     *
     * @param cursor
     * @return
     */
    public getgroupdetails getGroupDetails(Cursor cursor){
        getgroupdetails getgroupdetails = new getgroupdetails();
        getgroupdetails.setmGroupId(cursor.getString(0));
        getgroupdetails.setmGroupName(cursor.getString(1));
        getgroupdetails.setmGroupicon(cursor.getString(2));
        return getgroupdetails;
    }

}
