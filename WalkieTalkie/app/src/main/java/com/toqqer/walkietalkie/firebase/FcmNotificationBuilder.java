package com.toqqer.walkietalkie.firebase;


import android.util.Log;
import android.widget.Toast;

import com.toqqer.walkietalkie.database.AppSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by lenovo on 10/27/2017.
 */

public class FcmNotificationBuilder {
    public final static String AUTH_KEY_FCM = "AIzaSyC6RlVHfMnphAAVwrdQ0c2zGXlAD248Jbo";
    public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    public static void pushFCMNotification(final String deviceToken, final String title, final String body)
            throws IOException, JSONException {
        final String authKey = AUTH_KEY_FCM;   // You FCM AUTH key
        final String FMCurl = API_URL_FCM;

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try  {
                    URL url = new URL(FMCurl);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                    conn.setUseCaches(false);
                    conn.setDoInput(true);
                    conn.setDoOutput(true);

                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Authorization","key="+authKey);
                    conn.setRequestProperty("Content-Type","application/json");

                    JSONObject json = new JSONObject();
                    json.put("to",deviceToken.trim());
                    JSONObject info = new JSONObject();
                    info.put("title", title);   // Notification title
                    info.put("body", body); // Notification body
                    json.put("notification", info);

                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write(json.toString());
                    wr.flush();
                    conn.getInputStream();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

    }
}
