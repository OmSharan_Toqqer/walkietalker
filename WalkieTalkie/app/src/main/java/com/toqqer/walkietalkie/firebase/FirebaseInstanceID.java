package com.toqqer.walkietalkie.firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.toqqer.walkietalkie.database.AppSharedPreferences;

/**
 * Created by lenovo on 9/27/2017.
 */

public class FirebaseInstanceID extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseInstanceIDservice";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Tokens:" + refreshedToken);

        if (refreshedToken!=null){
            AppSharedPreferences.getInstance().saveFirebaseId(refreshedToken);
        }
    }
}
