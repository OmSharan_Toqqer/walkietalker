package com.toqqer.walkietalkie.group;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.toqqer.walkietalkie.R;
import com.toqqer.walkietalkie.adapter.ChatListUserAdapter;
import com.toqqer.walkietalkie.database.AppSharedPreferences;
import com.toqqer.walkietalkie.model.FilePath;
import com.toqqer.walkietalkie.model.User;
import com.toqqer.walkietalkie.ui.CropSquareTransformation;
import com.toqqer.walkietalkie.utils.UUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by lenovo on 10/17/2017.
 */

public class CreateGroupActivity extends AppCompatActivity implements View.OnClickListener {
    private int PROFILE_CAMERA = 1707;
    private int PROFILE_GALARY = 1708;

    private Toolbar mToolbar;
    private RecyclerView mRecyclerview;
    private RecyclerView.LayoutManager mLayoutManager;
    private EditText mGroupName;
    private ImageView mGroupIcon;

    private ProgressDialog mDilog;

    private FloatingActionButton Btn_Gallery,Btn_Camera,Btn_Remove;
    private BottomSheetDialog mBottomsheetDilog;

    private boolean result=false;
    private String userChoosenTask = null;
    private String selectedImagePath = null;

    private ArrayList<User> selectedgroupContacts;
    private Set<String> listIDChoose;
    private Set<String> listNumChoose;
    private String ContactsNames;

    private ChatListUserAdapter userAdapter;
    private Group groupEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_create);
        initilizeviews();
        mDilog = new ProgressDialog(CreateGroupActivity.this);
        mDilog.setCancelable(false);
        listIDChoose = new HashSet<>();
        listNumChoose = new HashSet<>();

        listIDChoose.add(AppSharedPreferences.getInstance().getUserId());
        listNumChoose.add(AppSharedPreferences.getInstance().getUserMobileNumber());

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            selectedgroupContacts = (ArrayList<User>)bundle.getSerializable("GROUP_CONTACTS");
            StringBuilder allContactNames = new StringBuilder();
            if (selectedgroupContacts!=null){
                for (int i=0;i<selectedgroupContacts.size();i++){
                    listIDChoose.add(selectedgroupContacts.get(i).getUserId());
                    listNumChoose.add(selectedgroupContacts.get(i).getUserContact());
                    if (i<selectedgroupContacts.size()-1) {
                        allContactNames.append(selectedgroupContacts.get(i).getUserName()+",");
                    }else {
                        allContactNames.append(selectedgroupContacts.get(i).getUserName());
                    }
                }
                ContactsNames = allContactNames.toString();

                mRecyclerview.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(getApplicationContext());
                mRecyclerview.setLayoutManager(mLayoutManager);
                userAdapter = new ChatListUserAdapter(selectedgroupContacts,this);
                mRecyclerview.setAdapter(userAdapter);
            }
        }

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mGroupIcon.setOnClickListener(this);
    }
    private void initilizeviews(){
        mToolbar = (Toolbar)findViewById(R.id.groupwindow_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("New getgroupdetails");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mRecyclerview = (RecyclerView)findViewById(R.id.mRecyclerviewgroup);
        mGroupIcon = (ImageView)findViewById(R.id.mgroup_icon);
        mGroupName = (EditText)findViewById(R.id.mgroup_name);

        initilize_bottomsheet();
    }
    private void initilize_bottomsheet(){
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.bottomsheetlayout, null);
        Btn_Gallery = (FloatingActionButton)modalbottomsheet.findViewById(R.id.fab_gallary);
        Btn_Camera = (FloatingActionButton)modalbottomsheet.findViewById(R.id.fab_camera);
        Btn_Remove = (FloatingActionButton)modalbottomsheet.findViewById(R.id.fab_remove);
        mBottomsheetDilog = new BottomSheetDialog(CreateGroupActivity.this);
        mBottomsheetDilog.setContentView(modalbottomsheet);
        mBottomsheetDilog.setCanceledOnTouchOutside(true);
        mBottomsheetDilog.setCancelable(true);

        Btn_Remove.setOnClickListener(this);
        Btn_Camera.setOnClickListener(this);
        Btn_Gallery.setOnClickListener(this);

    }
    //select Gallery
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),PROFILE_GALARY);
    }
    //select camera
    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, PROFILE_CAMERA);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case UUtils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Camera"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Gallery"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PROFILE_GALARY)
                onSelectFromGalleryResult(data);
            else if (requestCode == PROFILE_CAMERA)
                onCaptureImageResult(data);
        }
    }
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        selectedImagePath = FilePath.getPath(this, data.getData());
        Picasso.with(getApplicationContext())
                .load(data.getData())
                .transform(new CropSquareTransformation())
                .placeholder(R.drawable.image_holder)
                .error(R.drawable.image_holder)
                .into(mGroupIcon);
        if (mGroupIcon.getVisibility() != View.VISIBLE) {
            mGroupIcon.setVisibility(View.VISIBLE);
        }
    }
    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Picasso.with(getApplicationContext())
                .load(destination.getAbsoluteFile())
                .transform(new CropSquareTransformation())
                .placeholder(R.drawable.image_holder)
                .error(R.drawable.image_holder)
                .into(mGroupIcon);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_window_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_ok) {
            if (mGroupName.getText().toString().isEmpty()) {
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please Enter getgroupdetails Name", Snackbar.LENGTH_LONG);
                snackbar.show();
            }else {
                mDilog.setMessage("Please wait ...");
                showDialog();
                Creategroup();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //showng The Progress Dilog
    private void showDialog() {
        if (!mDilog.isShowing())
            mDilog.show();
    }
    //hide the Progress dilog
    public void hideDialog() {
        if (mDilog.isShowing())
            mDilog.dismiss();
    }
    private void Creategroup(){
        final String idGroup = (AppSharedPreferences.getInstance().getUserId()+ System.currentTimeMillis()).hashCode() + "";
        Room room = new Room();
        for (String id : listIDChoose ) {
            room.member.add(id);
        }
        room.groupInfo.put("name", mGroupName.getText().toString());
        room.groupInfo.put("admin", AppSharedPreferences.getInstance().getUserId());
        FirebaseDatabase.getInstance().getReference().child("group/" + idGroup).setValue(room).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                addRoomForUser(idGroup, 0);
            }
        });
    }
    private void editGroup() {

        //Delete group
        final String idGroup = groupEdit.id;
        Room room = new Room();
        for (String id : listIDChoose) {
            room.member.add(id);
        }
        room.groupInfo.put("name", mGroupName.getText().toString());
        room.groupInfo.put("admin", AppSharedPreferences.getInstance().getUserId());
        FirebaseDatabase.getInstance().getReference().child("group/" + idGroup).setValue(room)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        addRoomForUser(idGroup, 0);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                })
        ;
    }
    private void addRoomForUser(final String roomId, final int userIndex) {
        if (userIndex == listIDChoose.size()) {
            hideDialog();
            Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
           /* if (!isEditGroup) {
                dialogWait.dismiss();
                Toast.makeText(this, "Create group success", Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK, null);
                AddGroupActivity.this.finish();
            } else {
                deleteRoomForUser(roomId, 0);
            }*/
        } else {
            FirebaseDatabase.getInstance().getReference().child("users/" + listNumChoose.toArray()[userIndex] + "/group/" + roomId).setValue(roomId).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    addRoomForUser(roomId, userIndex + 1);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(CreateGroupActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        if (v == mGroupIcon){
            mBottomsheetDilog.show();
        }else if (v == Btn_Gallery){
            userChoosenTask = "Gallery";
            result =  UUtils.checkPermission(CreateGroupActivity.this);
            if (result)
                galleryIntent();
            mBottomsheetDilog.dismiss();
        }else if (v == Btn_Camera){
            userChoosenTask = "Camera";
            result =  UUtils.checkCameraPermission(CreateGroupActivity.this);
            if (result)
                cameraIntent();
            mBottomsheetDilog.dismiss();
        }else if (v == Btn_Remove){
            Toast.makeText(this, "Under Process", Toast.LENGTH_SHORT).show();
            mBottomsheetDilog.dismiss();
        }
    }
}
