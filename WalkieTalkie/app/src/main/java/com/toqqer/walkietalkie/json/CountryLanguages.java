package com.toqqer.walkietalkie.json;

/**
 * Created by lenovo on 9/25/2017.
 */

public class CountryLanguages {
    public static final String mLanguages = "[  \n" +
            "   {  \n" +
            "      \"Language\":\"English\",\n" +
            "      \"Locale\":\"en\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"తెలుగు\",\n" +
            "      \"Locale\":\"te\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"हिंदी\",\n" +
            "      \"Locale\":\"hi\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"français\",\n" +
            "      \"Locale\":\"fr\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"Deutsche\",\n" +
            "      \"Locale\":\"de\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"中文\",\n" +
            "      \"Locale\":\"zh\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"čeština\",\n" +
            "      \"Locale\":\"cs\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"Nederlands\",\n" +
            "      \"Locale\":\"nl\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"italiano\",\n" +
            "      \"Locale\":\"it\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"日本語\",\n" +
            "      \"Locale\":\"ja\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"한국어\",\n" +
            "      \"Locale\":\"ko\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"Español\",\n" +
            "      \"Locale\":\"es\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"русский\",\n" +
            "      \"Locale\":\"ru\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"Ελληνικά\",\n" +
            "      \"Locale\":\"el\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"Українська\",\n" +
            "      \"Locale\":\"uk\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"Română\",\n" +
            "      \"Locale\":\"ro\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"Português\",\n" +
            "      \"Locale\":\"pt\"\n" +
            "   },\n" +
            "   {  \n" +
            "      \"Language\":\"bahasa Indonesia\",\n" +
            "      \"Locale\":\"in\"\n" +
            "   }\n" +
            "]";
}
