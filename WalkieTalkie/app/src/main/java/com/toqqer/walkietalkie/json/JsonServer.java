package com.toqqer.walkietalkie.json;

import android.content.Context;

import com.toqqer.walkietalkie.network.GetAllContactsService;
import com.toqqer.walkietalkie.network.RegisterService;

/**
 * Created by Om on 09-10-2017.
 */

public class JsonServer {

    public static void getRegisteredContacts(Context context,GetAllContactsService.AllRegisteredContactsListener registeredContactsListener){
        GetAllContactsService allContactService = new GetAllContactsService(context, registeredContactsListener);
        allContactService.execute();
       // return allContactService.getAllRegisteredContacts();
    }

   /* private static List<User> getProfileInfo(Context context){
        GetAllContactsService allContactService = new GetAllContactsService();
        allContactService.execute();
       // return allContactService.getAllRegisteredContacts();
    }*/

    public static void getRegistrationService(Context context, RegisterService.RegisteredUserListener registeredUserListener){
        RegisterService registerService = new RegisterService(context, registeredUserListener);
        registerService.execute();
        // return allContactService.getAllRegisteredContacts();
    }
}
