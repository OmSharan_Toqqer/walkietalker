package com.toqqer.walkietalkie.model;

/**
 * Created by lenovo on 10/9/2017.
 */

public class ChatMessages {
    private String audioFilePath;
    private String userId;
    private int viewType;

    public ChatMessages(String aAudiMessagePath,String auserId,int aViewType){
        this.audioFilePath = aAudiMessagePath;
        this.userId = auserId;
        this.viewType = aViewType;
    }



    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(long messageTime) {
        this.messageTime = messageTime;
    }

    private long messageTime;

    public void setAudioFilePath(String audioFilePath) {
        this.audioFilePath = audioFilePath;
    }

    public String getAudioFilePath() {

        return audioFilePath;
    }

}
