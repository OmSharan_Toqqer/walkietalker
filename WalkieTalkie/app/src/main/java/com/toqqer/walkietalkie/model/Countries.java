package com.toqqer.walkietalkie.model;

/**
 * Created by lenovo on 9/19/2017.
 */

public class Countries {
    private String Name;
    private String Code;

    public Countries(String aName,String aCode){
        this.Name = aName;
        this.Code = aCode;
    }

    public void setName(String name) {
        Name = name;
    }
    public void setCode(String code) {
        Code = code;
    }
    public String getName() {
        return Name;
    }

    public String getCode() {
        return Code;
    }


    @Override
    public String toString() {
        return "Countries{" +
                "Name='" + Name + '\'' +
                ", Code='" + Code + '\'' +
                '}';
    }
}
