package com.toqqer.walkietalkie.model;

/**
 * Created by lenovo on 9/25/2017.
 */

public class Languages {
    private String mLanguage;
    private String mLocale;

    public Languages(String aLng,String aLocale){
        this.mLanguage = aLng;
        this.mLocale = aLocale;
    }

    public String getmLanguage() {
        return mLanguage;
    }

    public void setmLanguage(String mLanguage) {
        this.mLanguage = mLanguage;
    }

    public String getmLocale() {
        return mLocale;
    }

    public void setmLocale(String mLocale) {
        this.mLocale = mLocale;
    }
}
