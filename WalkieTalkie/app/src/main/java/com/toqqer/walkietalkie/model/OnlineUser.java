package com.toqqer.walkietalkie.model;

/**
 * Created by lenovo on 9/25/2017.
 */

public class OnlineUser {
    private String name;
    private int imageSource;
    public OnlineUser (int imageSource, String name) {
        this.name = name;
        this.imageSource = imageSource;
    }

    public String getName() {
        return name;
    }

    public int getImageSource() {
        return imageSource;
    }

}
