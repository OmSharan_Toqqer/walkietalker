package com.toqqer.walkietalkie.model;

/**
 * Created by Ranjeet on 9/22/2017.
 */

public class PeopleListData {

    private String userName, lastMessage, lastMsgTime, unreadMessage;
    private int chatProfileImg;

    public PeopleListData(int chatProfileImg,String userName, String lastMessage, String lastMsgTime, String unreadMessage ) {
        this.chatProfileImg = chatProfileImg;
        this.userName = userName;
        this.lastMessage = lastMessage;
        this.lastMsgTime = lastMsgTime;
        this.unreadMessage = unreadMessage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMsgTime() {
        return lastMsgTime;
    }

    public void setLastMsgTime(String lastMsgTime) {
        this.lastMsgTime = lastMsgTime;
    }

    public String getUnreadMessage() {
        return unreadMessage;
    }

    public void setUnreadMessage(String unreadMessage) {
        this.unreadMessage = unreadMessage;
    }

    public int getChatProfileImg() {
        return chatProfileImg;
    }

    public void setChatProfileImg(int chatProfileImg) {
        this.chatProfileImg = chatProfileImg;
    }
}
