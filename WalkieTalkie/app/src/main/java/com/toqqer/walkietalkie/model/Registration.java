package com.toqqer.walkietalkie.model;

/**
 * Created by Om on 10-10-2017.
 */

public class Registration {
    String mStatus;
    String mMessage;
    String mUserID;

    public Registration(String mStatus, String mMessage, String mUserID) {
        this.mStatus = mStatus;
        this.mMessage = mMessage;
        this.mUserID = mUserID;
    }

    public Registration() {
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String mUserID) {
        this.mUserID = mUserID;
    }
}
