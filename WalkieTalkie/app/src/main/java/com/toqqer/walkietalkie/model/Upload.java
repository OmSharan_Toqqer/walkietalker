package com.toqqer.walkietalkie.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by lenovo on 10/25/2017.
 */
@IgnoreExtraProperties
public class Upload {
    private String deviceId;
    private String mobilenumber;
    private String firebaseid;
    private String name;
    private String url;
    private String userId;
    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public Upload() {
    }

    public Upload(String name, String url,String deviceId,String mobilenumber,String firebaseid,String userId) {
        this.deviceId = deviceId;
        this.mobilenumber = mobilenumber;
        this.firebaseid = firebaseid;
        this.name = name;
        this.url= url;
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public String getFirebaseid() {
        return firebaseid;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getUserId() {
        return userId;
    }
}
