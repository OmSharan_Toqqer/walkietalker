package com.toqqer.walkietalkie.model;

import java.io.Serializable;

/**
 * Created by Om on 09-10-2017.
 */

public class User implements Serializable {
    private String dbId;
    private String userId;
    private String thumbImg;
    private String userContact;
    private String userName;
    private String user_firebase_token;
    private int isGroupValue;

    public User() {
    }

    public User(String userId,String firebase_token,String thumbImg, String userContact, String userName) {
        this.userId = userId;
        this.user_firebase_token = firebase_token;
        this.thumbImg = thumbImg;
        this.userContact = userContact;
        this.userName = userName;
    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String dbId) {
        this.dbId = dbId;
    }

    public String getUser_firebase_token() {
        return user_firebase_token;
    }

    public void setUser_firebase_token(String user_firebase_token) {
        this.user_firebase_token = user_firebase_token;
    }

    public int getIsGroupValue() {
        return isGroupValue;
    }

    public void setIsGroupValue(int isGroupValue) {
        this.isGroupValue = isGroupValue;
    }

    public String getUserContact() {
        return userContact;
    }

    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    public String getUserId() {
        return userId;

    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getThumbImg() {
        return thumbImg;
    }

    public void setThumbImg(String thumbImg) {
        this.thumbImg = thumbImg;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
