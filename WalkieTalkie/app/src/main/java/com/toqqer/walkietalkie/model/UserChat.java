package com.toqqer.walkietalkie.model;

/**
 * Created by lenovo on 10/18/2017.
 */

public class UserChat {
    private String senderUId;
    private String receiverUId;
    private String message;
    private int messageId;
    private String messageTxt;
    private String mCurrentTime;
    private int mstatus;
    private String mGroupId;
    private String mGroupname;

    public UserChat() {

    }
    public UserChat(String senderUId,String receiverUId,String message,int messageId,String messageTxt,String currenttime,int mstatus){
        this.senderUId = senderUId;
        this.receiverUId = receiverUId;
        this.message = message;
        this.messageId = messageId;
        this.messageTxt = messageTxt;
        this.mCurrentTime = currenttime;
        this.mstatus = mstatus;
    }

    public String getmGroupId() {
        return mGroupId;
    }

    public void setmGroupId(String mGroupId) {
        this.mGroupId = mGroupId;
    }

    public String getmGroupname() {
        return mGroupname;
    }

    public void setmGroupname(String mGroupname) {
        this.mGroupname = mGroupname;
    }

    public String getmCurrentTime() {
        return mCurrentTime;
    }

    public void setmCurrentTime(String mCurrentTime) {
        this.mCurrentTime = mCurrentTime;
    }

    public String getSenderUId() {
        return senderUId;
    }

    public void setSenderUId(String senderUId) {
        this.senderUId = senderUId;
    }

    public String getReceiverUId() {
        return receiverUId;
    }

    public void setReceiverUId(String receiverUId) {
        this.receiverUId = receiverUId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageTxt() {
        return messageTxt;
    }

    public void setMessageTxt(String messageTxt) {
        this.messageTxt = messageTxt;
    }

    public int getMstatus() {
        return mstatus;
    }

    public void setMstatus(int mstatus) {
        this.mstatus = mstatus;
    }
}
