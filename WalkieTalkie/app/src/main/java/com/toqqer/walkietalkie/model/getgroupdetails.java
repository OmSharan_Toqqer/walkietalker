package com.toqqer.walkietalkie.model;

/**
 * Created by lenovo on 10/17/2017.
 */

public class getgroupdetails {
    private String mGroupName;
    private String mGroupicon;
    private String mGroupId;
    private String mUserIds;

    public String getmGroupName() {
        return mGroupName;
    }

    public void setmGroupName(String mGroupName) {
        this.mGroupName = mGroupName;
    }

    public String getmGroupicon() {
        return mGroupicon;
    }

    public void setmGroupicon(String mGroupicon) {
        this.mGroupicon = mGroupicon;
    }

    public String getmGroupId() {
        return mGroupId;
    }

    public void setmGroupId(String mGroupId) {
        this.mGroupId = mGroupId;
    }

    public String getmUserIds() {
        return mUserIds;
    }

    public void setmUserIds(String mUserIds) {
        this.mUserIds = mUserIds;
    }
}
