package com.toqqer.walkietalkie.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.toqqer.walkietalkie.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OM on 09-10-2017.
 */

public class GetAllContactsService extends AsyncTask<Void, Void, Void> {

    private List<User> userList;
    private Context context;
    private AllRegisteredContactsListener registeredContactsListener;
    private ProgressDialog pDialog;
    private JSONObject jsonObject;
    private String strParsedValue;
    private String strJSONValue = "{\"status\":\"200\" ,\"message\":\"Selected Contacts\","
            + " \"data\":[ " +
            "{\"user_id\":\"1\" ,\"phone_number\":\"9491430751\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"2\" ,\"phone_number\":\"9827453646\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"3\" ,\"phone_number\":\"9321342312\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"4\" ,\"phone_number\":\"9234567891\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"5\" ,\"phone_number\":\"8867345623\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"6\" ,\"phone_number\":\"7889123456\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"7\" ,\"phone_number\":\"9415234578\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"8\" ,\"phone_number\":\"9491430753\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"9\" ,\"phone_number\":\"9491430759\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"10\" ,\"phone_number\":\"9491430758\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"11\" ,\"phone_number\":\"9491430752\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"12\" ,\"phone_number\":\"9491430754\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"13\" ,\"phone_number\":\"9491430755\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"14\" ,\"phone_number\":\"9491430756\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"15\" ,\"phone_number\":\"9491430750\" ,\"profile_image\":\"ImageFile\"}," +
            "{\"user_id\":\"16\" ,\"phone_number\":\"9491430734\" ,\"profile_image\":\"ImageFile\"}," +
            "]" +
            "}";


    public GetAllContactsService(Context context, AllRegisteredContactsListener registeredContactsListener) {
        this.context = context;
        this.registeredContactsListener = registeredContactsListener;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

    }

    @Override
    protected Void doInBackground(Void... voids) {

        parseJson(strJSONValue);

        return null;
    }


    private List<User> parseJson(String strJSONValue) {

        try {
            jsonObject = new JSONObject(strJSONValue);


            // JSONObject object = jsonObject.getJSONObject("status");
            String status = jsonObject.getString("status");
            String msg = jsonObject.getString("message");

            strParsedValue = "Status value => " + status;
            strParsedValue += "\n Message value => " + msg;

            //JSONObject subObject = object.getJSONObject("sub");
            JSONArray jsonArray = jsonObject.getJSONArray("data");

            strParsedValue += "\n Array Length => " + jsonArray.length();
            userList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                User user = new User();
                user.setUserId(jsonObject.getString("user_id"));
                user.setUserContact(jsonObject.getString("phone_number"));
                user.setThumbImg(jsonObject.getString("profile_image"));

                userList.add(user);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        pDialog.dismiss();
        registeredContactsListener.setAllRegisteredContacts(userList);
        // this.userList = contactsList;
        //textView.setText("Result is:- \n" + userId+);
       /* textView.setText("Result is:- \n");
        for (User user : userList) {
            textView.append(user.getId() + " " + user.getSenderUId() + " " + user.getMobile() + "\n");
        }

        // Close Database
        dbHandler.close();*/
    }


    /*public List<User> getAllRegisteredContacts(){
        return userList;
    }*/

    public interface AllRegisteredContactsListener {
               public void setAllRegisteredContacts(List<User> contactList);
    }
}
