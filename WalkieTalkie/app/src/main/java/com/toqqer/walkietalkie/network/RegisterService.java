package com.toqqer.walkietalkie.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.toqqer.walkietalkie.model.Registration;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Om on 10-10-2017.
 */

public class RegisterService extends AsyncTask<Void, Void, Registration> {

    private String strJSONValue = "{\"status\":\"200\" ,\"message\":\"registration successful\", \"user_id\": \"1\"}";
    private Context context;
    private RegisterService.RegisteredUserListener registeredUserListener;
    private ProgressDialog pDialog;
    private JSONObject jsonObject;


    public RegisterService(Context context, RegisteredUserListener registeredUserListener) {
        this.context = context;
        this.registeredUserListener = registeredUserListener;
    }



  /*  @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Getting Data ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

    }*/


    @Override
    protected Registration doInBackground(Void... voids) {

        Registration registration = parseJson(strJSONValue);

        return registration;
    }



    private Registration parseJson(String strJSONValue) {
        try {
            jsonObject = new JSONObject(strJSONValue);
            Registration registration = new Registration();

            registration.setUserID(jsonObject.getString("user_id"));
            registration.setMessage(jsonObject.getString("message"));
            registration.setStatus(jsonObject.getString("status"));

            return registration;

        }catch (JSONException e) {
            e.printStackTrace();
        }
           return null;
    }

    @Override
    protected void onPostExecute(Registration registration) {
      //  pDialog.dismiss();
        registeredUserListener.getUserIDListener(registration);

    }

    public interface RegisteredUserListener {
        public void getUserIDListener(Registration registration);
    }
}
